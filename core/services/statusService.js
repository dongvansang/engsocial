module.exports = function (mongoose, models) {
	var async = require('async'),
		helper = require('../helper')(),
		_ = require('underscore');

	function getStatuses(userId, amount, page, done) {
		models.User.findById(userId
		, function (err, foundUser) {
			if (err) {
				done(err, null);
			} else {
				var lengthStatuses = foundUser.statuses.length,
					amountStatuses,
					statuses = [],
					// choose correct note then push into this result
					result = [];
					
				// it is invalid
				if (lengthStatuses - ((page-1) * amount) < 0) {
					done(null, []);
				} else {
					amountStatuses = lengthStatuses - ((page-1) * amount) < amount ? lengthStatuses - ((page-1) * amount) : amount;

					statuses = foundUser.statuses.splice(lengthStatuses - (page-1) * amount - amountStatuses, amountStatuses);

					async.each(statuses, iterator, function (err) {
						result = _(result).sortBy(function (status) {
							return status.createdIn;
						});
						done(err, result);
					});

					function iterator (statusId, done) {
						models.Post.findById(statusId
						, function (err, foundPost) {
							if (err) {
								done(err);
							} else {
								if(foundPost !== null) {
									var statusDomain = toStatusDomain(foundUser, foundPost);
									result.push(statusDomain);
								}
								done();
							}
						});
					}// end iterator function
				}
			}
		});
	}

	function addStatus(userId, content, done) {
		var post = new models.Post();
		post.content = _.escape(content);
		post.typeObj = 'status';
		post.owner = userId;
		post.save(function (err, savedPost) {
			models.User.findByIdAndUpdate(userId
			, { $push: { statuses: savedPost._id }
				, $set: { currentStatus: (content.slice(0, 100) + "...") } }
			, function (err, savedUser) {
				if (err) { done(err, null); }
				else {
					var statusDomain = toStatusDomain(savedUser, savedPost);
					done(null, statusDomain);
				}
			});
		});
	}

	function toStatusDomain (owner, post) {
		var statusDomain = {
			id: post._id,
			nickname: owner.nickname,
			avatar: owner.avatar,
			content: post.content,
			likes: post.likes || [],
			createdIn: post.createdIn,
			comments: post.statusObj.comments || [],
			rates: post.statusObj.rates || [],
			edits: post.statusObj.edits || []
		}

		return statusDomain;
	}

	return {
		getStatuses: getStatuses,
		addStatus: addStatus,
		toStatusDomain: toStatusDomain
	}
}