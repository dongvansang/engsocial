module.exports = function (mongoose, models) {
	var async = require('async'),
		_ = require('underscore');

	function addFriend (rootUserId, userId, point, done) {
		// add friend that happen both user
		var friendShip = {
			userId: userId,
			point: point
		},
		refriendShip = {
			userId: rootUserId,
			point: point
		};

		// push friendship into rootUser
		models.User.findByIdAndUpdate(rootUserId
		, { $push: { friends: friendShip } }
		, function (err, savedRootUser) {
			if (err) {
				done(err, null);
			} else {
				// push friendship into userId
				models.User.findByIdAndUpdate(userId
				, { $push: { friends: refriendShip } }
				, function (err, savedUser) {
					if (err) {
						done(err, null);
					} else {
						done(null, 'Add friend successfully');
					}
				});
			}
		});
	}

	function unFriend (rootUserId, userId, done) {
		// do parallel tasks
		async.parallel([
			// update rootUserId
			function (done) {
				models.User.findByIdAndUpdate(rootUserId
				, { $pull: { friends: { userId: userId } } }
				, function (err, savedRootUser) {
					if (err) {
						done(err);
					} else {
						done(null, savedRootUser.nickname + ' removed ' + userId + ' from list friend');
					}
				});
			},
			// update userId
			function (done) {
				models.User.findByIdAndUpdate(userId
				, { $pull: { friends: { userId: rootUserId } } }
				, function (err, savedUser) {
					if (err) {
						done(err);
					} else {
						done(null, savedUser.nickname + ' removed ' + rootUserId + ' from list friend');
					}
				});
			}
		], done ) // call done callback when 2 task finished
		// end parallel perform
	}

	function getFriendsId(userId, done) {
		models.User.findById(userId, 'friends', function (err, user) {
			done(err, user.friends);
		});
	}

	function getFriends (userId, amount, done) {
		// array of friendDomain
		var friends = [];

		var iterator = function (friend, iteratorDone) {
			// get more details about userId
			// select only specify field
			models.User.findById(friend.userId
			, 'nickname avatar registerIn rateHistories currentStatus'
			, function (err, foundFriendUser) {
				if (err) {
					iteratorDone(err);
				} else {
					var friendDomain = {
						id: foundFriendUser._id,
						nickname: foundFriendUser.nickname,
						avatar: foundFriendUser.avatar,
						currentStatus: foundFriendUser.currentStatus,
						registerIn: foundFriendUser.registerIn
					}
					
					friendDomain.scoreObj = _.reduce(foundFriendUser.rateHistories
					, function(memo, rateHistory) {
						if (memo.amount === 0) {
							return {
								score: rateHistory.score,
								amount: rateHistory.amount
							}
						} else {
	  					return { 
	  						score: ( ((rateHistory.score * rateHistory.amount) + (memo.score * memo.amount)) / (rateHistory.amount + memo.amount) ).toFixed(1),
	  						amount: rateHistory.amount + memo.amount
	  					}
	  				}
					}, { score: 0, amount: 0 });

					if (friendDomain.scoreObj.score >= 2.8) {
						friendDomain.level = 'intermediate';
					} else {
						if (friendDomain.scoreObj.score >= 1.2) {
							friendDomain.level = 'advance';
						} else {
							friendDomain.level = 'beginner';
						}
					}

					friendDomain.formatedRegisterIn = foundFriendUser.registerIn.toString().slice(0, 24);

					friends.push(friendDomain);	
					iteratorDone();
				}
			});
		}

		models.User.findById(userId, function (err, foundUser) {
			if (err) {
				done(err, null);
			} else {
				// iterator of user.friends each function
				async.each(foundUser.friends, iterator, function (err) {
					if (err) {
						done(err,  null);
					} else {
						// return friends
						done(null, friends);
					}
				});
			}
		});
	}

	return {
		addFriend: addFriend,
		unFriend: unFriend,
		getFriends: getFriends,
		getFriendsId: getFriendsId
	}
}