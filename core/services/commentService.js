/**
 * Expose CommentService instance
 */
module.exports = function (mongoose, models) {
	var async = require('async'),
		helper = require('../helper')(),
		_ = require('underscore');

	/**
	 * Add a comment inside status 
	 *
	 * @param {String} userId
	 * @param {String} content
	 * @param {String} parent
	 * @param {Boolean} isStatus
	 * @param {Function} done
	 */
	function addComment(userId, content, parent, isStatus, done) {
		// create a new comment
		var post = new models.Post();
		post.content = _.escape(content);
		post.owner = userId;
		post.typeObj = 'comment';
		post.commentObj.parent = parent;
		post.save(function (err, savedComment) {
			// add new comment to list comments in status object
			models.Post.findById(parent, function (err, foundPost) {
				if (err) {
					done(err, null);
				} else {
					// add reference to parent post
					if (isStatus === 'true') { // if it's a status
						foundPost.statusObj.comments.push(savedComment._id);
					} else {
						foundPost.noteObj.comments.push(savedComment._id);	
					}
					
					foundPost.save(function (err, savedPost) {
						if (err) {
							done(err, null);
						} else {
							models.User.findById(userId, function (err, user) {
								if (err) {
									done(err, null);
								} else {
									var commentDomain = toCommentDomain(user, savedComment, parent);
									done(null, commentDomain);
								}
							});
						}
					});
				}
			});
		});
	}

	/** 
	 * Get amount of post's comment
	 * 
	 * @param {String} postId
	 * @param {Number} amount
	 * @param {Number} page
	 * @param {Function} done
	 */
	function getComments(postId, amount, page, done) {
		models.Post.findById(postId, function (err, foundPost) {
			if (err) {
				done(err, null);
			} else {

				var commentsId, //list comment of a status
					lengthComments, // length of list comment
					amountComments, // amount of comment that can get
					result = [];

				if (foundPost.typeObj === 'status')
					commentsId = foundPost.statusObj.comments;
				else commentsId = foundPost.noteObj.comments;

				lengthComments = commentsId.length;

				// it is invalid
				if (lengthComments - ((page-1) * amount) < 0) {
					done(null, []);
					return;
				}

				amountComments = lengthComments - ((page-1) * amount) < amount ? 	lengthComments - ((page-1) * amount) : amount;
				
				commentsId = commentsId.splice(lengthComments - (page-1) * amount - amountComments, amountComments);

				async.each(commentsId, iterator, function (err) {
					result = _(result).sortBy(function (comment) {
						return comment.createdIn;
					});
					result = result.reverse();
					done(err, result);
				});
				
				// Map list comments id to list CommentDomain
				function iterator (commentId, done) {
					models.Post.findById(commentId, function (err, foundPost) {
						if (err) { done(err, null); }
						else {
							models.User.findById(foundPost.owner, function (err, foundUser) {
								if (err) {
									done(err, null);
								} else {
									var commentDomain = toCommentDomain(foundUser, foundPost, postId);
									result.push(commentDomain);
									done();
								} 
							});
						}
					});
				}
			}
		});
	}

	function toCommentDomain (user, post, parent) {
		var len = post.commentObj.rates.length,
			sum = 0,
			ratingScore = 0,
			ratingTotalScore = 0;

		// if you haven't yet rated, score is zero
		// else return your rating score
		for (var i = 0; i < len; i++) {
			if (post.commentObj.rates[i].userId.toString() === user._id.toString()) {
				ratingScore = post.commentObj.rates[i].score;
			}
			sum += post.commentObj.rates[i].score;
		}

		// 1.213 => 1.251628a53a722414f1c000004
		if (sum !== 0) {
			ratingTotalScore = (sum/len).toFixed(1);
		}

		var commentDomain = {
			id: post._id,
			parent: parent,
			nickname: user.nickname,
			avatar: user.avatar,
			content: post.content,
			likes: post.likes,
			ratingScore: ratingScore,
			ratingTotalScore: ratingTotalScore,
			rates: post.commentObj.rates,
			createdIn: post.createdIn
		}

		return commentDomain;
	}

	return {
		addComment: addComment,
		getComments: getComments
	}
}