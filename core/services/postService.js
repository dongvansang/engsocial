/**
 * Expose PostService instance
 */
module.exports = function (mongoose, models) {
	var async = require('async'),
		_ = require('underscore');

	function like(userId, postId, done) {
		models.Post.findOne({ _id: postId }, function (err, post) {
			if (err) { console.log(err); }
			else {
				if (post.likes.indexOf(userId) === -1) {
					post.likes.push(userId);
					post.save(done);
				} else {
					done('Error: Like conflict', null);
				}
			}
		});
	}

	function unlike(userId, postId, done) {
		models.Post.findOne({ _id: postId }, function (err, post) {
			if (err) { console.log(err); }
			else {
				var index = post.likes.indexOf(userId);
				if (index === -1) {
					done(null, null);
				} else {
					post.likes.splice(index, 1);
					post.save(done);	
				}
			}
		});
	}

	function rate(userId, postId, score, done) {
		var oldScore;
		models.Post.findById(postId, onFoundPost);

		// get post by postId
		function onFoundPost (err, foundPost) {
			if (err) { done(err, null); }
			else {
				var isNewRating = true, rates = [];

				if (foundPost.typeObj === 'status') {
					rates = foundPost.statusObj.rates;
				} else if (foundPost.typeObj === 'comment') {
					rates = foundPost.commentObj.rates;
				} else {
					rates = foundPost.editObj.rates;
				}

				_(rates).each(function (rate, index) {
					if (rate.userId.toString() == userId) {
						oldScore = rate.score;
						if (score == 0) {
							rates.splice(index, 1);
						} else {
							rate.score = score;
							rate.lastModified = new Date();
						}
						isNewRating = false;
					}
				});

				// if this is new rating,
				// push into rates
				if (isNewRating === true && score != 0) {
					rates.push({ userId: userId, score: score });
				}

				// save post that modified rates array
				foundPost.save(onSavedPost);
				function onSavedPost(err, savedPost) {
					if (err) {
						done(err, null);
					} else {
						done(null, rates);

						// update rate histories
						var score = 0,
							rateLength = rates.length;

						if (rateLength !== 0) {
							_(rates).each(function (rate) {
								score += rate.score;
							});
							score = (score / rateLength).toFixed(1);	
						}

						models.User.findById(savedPost.owner
						, 'rateHistories'
						, function (err, foundUser) {
							if (err) {
								console.log(err);
							} else {
								var len = foundUser.rateHistories.length,
									isNew = true;
								for (var i = 0; i < len; i++) {
									if (foundUser.rateHistories[i].postId.toString() == postId) {
										if (score === 0) {
											// remove from array
											foundUser.rateHistories.splice(i, 1);
										} else {
											foundUser.rateHistories[i].score = score;
										foundUser.rateHistories[i].amount = rateLength;
										foundUser.rateHistories[i].lastModified = new Date();	
										}

										isNew = false;
										break;
									}
								}
								if (isNew) {
									foundUser.rateHistories.push({
										postId: postId,
										score: score,
										amount: 1
									});
								}
								foundUser.save();
							}
						});
					}
				}
			}
		}
	}

	function getLikeUsers(postId, done) {
		models.Post.findOne({ _id: postId }, function (err, foundPost) {
			var result = [];
			if (err) { console.log(err); }
			else {
				var postHandler = function (userId, done) {
					models.User.findOne({ _id: userId }, function (err, user) {
						if (err) { console.log(err); }
						else {
							result.push({
								_id: user._id,
								nickname: user.nickname,
								avatar: user.avatar
							});
							done();
						}
					});
				};
				async.each(foundPost.likes, postHandler, function () {
					done(null, result);
				});
			}
		});
	}

	function deletePost(userId, postId, done) {
		models.Post.findByIdAndRemove(postId, function (err, deletedPost) {
			if (err) { done(err, null); }
			else {
				if (deletedPost !== null) {
					// name of array must pull this postId
					var deleteArrayName = '';
					// in case typeObj === 'commnent' pull comment from parent post
					if (deletedPost.typeObj === 'comment') {
						models.Post.findByIdAndUpdate(deletedPost.commentObj.parent
						// remove deleted comment from comment list
						, { $pull: { 'statusObj.comments': postId, 'noteObj.comments': postId } }
						, function (err, foundPost) {
							if (err) {
								done(err, null);
							} else {
								done(null, deletedPost);
							}
						});
					} else {
						if (deletedPost.typeObj === 'status')
							deleteArrayName = 'statuses';
						else deleteArrayName = 'notes';

						models.User.findByIdAndUpdate(userId
						, { $pull: { deleteArrayName: postId } }, function (err, user) {
							if (err) {
								done(err, null);
							} else {
								done(null, deletedPost);
							}
						});
					}
				}
			}
		});
	}

	return {
		like: like,
		unlike: unlike,
		rate: rate,
		getLikeUsers: getLikeUsers,
		deletePost: deletePost
	}
}