/**
 * Expose CommentService instance
 */
module.exports = function (mongoose, models) {
	var async = require('async'),
		helper = require('../helper')(),
		_ = require('underscore');

	function addEdit(userId, content, parent, editText, editTo, done) {
		var post = new models.Post();
		post.content = _.escape(content);
		post.owner = userId;
		post.typeObj = 'edit';
		post.editObj.parent = parent;
		post.editObj.editText = editText;
		post.editObj.editTo = editTo;
		post.save(function (err, savedEdit) {
			models.Post.findById(parent, function (err, foundPost) {
				if (err) {
					done(err, null);
				} else {
					foundPost.statusObj.edits.push(savedEdit._id);
					foundPost.save(function (err, savedPost) {
						if (err) {
							done(err, null);
						} else {
							models.User.findById(userId, function (err, user) {
								if (err) {
									done(err, null);
								} else {
									var editDomain = toEditDomain(user, savedEdit, parent);
									done(null, editDomain);
								}
							});
						}
					});
				}
			});
		});
	}

	function getEdits(postId, amount, page, done) {
		models.Post.findById(postId, function (err, foundPost) {
			if (err) {
				done(err, null);
			} else {
				var editsId, //list edit of a status
					lengthEdits, // length of list edit
					amountEdits, // amount of edit that can get
					result = [];

				editsId = foundPost.statusObj.edits;
				lengthEdits = editsId.length;

				// it is invalid
				if (lengthEdits - ((page-1) * amount) < 0) {
					done(null, []);
					return;
				}

				amountEdits = lengthEdits - ((page-1) * amount) < amount ? 	lengthEdits - ((page-1) * amount) : amount;
				
				editsId = editsId.splice(lengthEdits - (page-1) * amount - amountEdits, amountEdits);

				async.each(editsId, iterator, function (err) {
					result = _(result).sortBy(function (edit) {
						return edit.createdIn;
					});
					done(err, result);
				});
				
				// Map list edits id to list EditDomain
				function iterator (editId, done) {
					models.Post.findById(editId, function (err, foundPost) {
						if (err) { done(err, null); }
						else {
							models.User.findById(foundPost.owner, function (err, foundUser) {
								if (err) {
									done(err, null);
								} else {
									var editDomain = toEditDomain(foundUser, foundPost, postId);
									result.push(editDomain);
									done();
								} 
							});
						}
					});
				}
			}
		});
	}

	function toEditDomain (user, post, parent) {
		var len = post.editObj.rates.length,
			sum = 0,
			ratingScore = 0,
			ratingTotalScore = 0;

		// if you haven't yet rated, score is zero
		// else return your rating score
		for (var i = 0; i < len; i++) {
			if (post.editObj.rates[i].userId.toString() === user._id.toString()) {
				ratingScore = post.editObj.rates[i].score;
			}
			sum += post.editObj.rates[i].score;
		}

		// 1.213 => 1.251628a53a722414f1c000004
		if (sum !== 0) {
			ratingTotalScore = (sum/len).toFixed(1);
		}

		var editDomain = {
			id: post._id,
			parent: parent,
			nickname: user.nickname,
			avatar: user.avatar,
			content: post.content,
			editText: post.editObj.editText,
			editTo: post.editObj.editTo,
			likes: post.likes,
			ratingScore: ratingScore,
			ratingTotalScore: ratingTotalScore,
			rates: post.editObj.rates,
			createdIn: post.createdIn
		}

		return editDomain;
	}

	return {
		addEdit: addEdit,
		getEdits: getEdits
	}
}