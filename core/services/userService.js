module.exports = function (mongoose, models) {
	var async = require('async'),
		_ = require('underscore');

	function register (email, nickname, encryptedPassword, done) {
		var randomNumber = Math.floor((Math.random()*30)+1);
		var user = new models.User({
			email: email,
			nickname: nickname,
			password: encryptedPassword,
			avatar: '/img/default-avatar.jpg'
		});

		// callback function 'done' with 2 parameter (err, user)
		user.save(function (err, savedUser) {
			if (err) {
				done(err,null);
			} 	else {
				if (typeof savedUser === 'undefined') {
					done('Can\'t not register: ' + user, null);
				} else {
					done(err, {
						_id: savedUser._id,
						nickname: savedUser.nickname,
						avatar: savedUser.avatar });
				}
			}
		});
	}

	function checkEmailExist(email, done) {
		models.User.findOne({ email: email }
		, 'email'
		, function (err, foundUser) {
			done(err, foundUser !== null)
		});
	}
    
  function getUserByEmailPassword (email, encryptedPassword, done) {
    models.User.findOne({ email: email, password: encryptedPassword }
    , '_id email nickname avatar'
    , function (err, user) {
        if (err) {
            done(err, null);
        } else {
            done(null, user);
        }
    });
  }

  function getShortProfile (nickname, done) {
		models.User.findOne({ nickname: nickname }
		, '_id nickname email birthday avatar address rateHistories registerIn'
		, function (err, foundUser) {
			if (err) {
				done(err, null);  
			} else {
				var user = {
					id: foundUser._id,
					nickname: foundUser.nickname,
					email: foundUser.email,
					avatar: foundUser.avatar,
					address: foundUser.address || 'unknown',
					rateHistories: foundUser.rateHistories
				};
				user.scoreObj = _.reduce(foundUser.rateHistories
				, function(memo, rateHistory) {
					if (memo.amount === 0) {
						return {
							score: rateHistory.score,
							amount: rateHistory.amount
						}
					} else {
						return {
							score: ( ((rateHistory.score * rateHistory.amount) + (memo.score * memo.amount)) / (rateHistory.amount + memo.amount) ).toFixed(1),
							amount: rateHistory.amount + memo.amount
						}
					}
				}, { score: 0, amount: 0 });

				if (user.scoreObj.score >= 2.8) {
					user.level = 'advanced';
				} else {
					if (user.scoreObj.score >= 1.2) {
						user.level = 'intermediate';
					} else {
						user.level = 'beginner';
					}
				}

				if (typeof foundUser.birthday === undefined)
					foundUser.birthday = 'unknown';
				else {
					user.birthday = foundUser.birthday.toString().slice(0, 15);
				} 

				user.registerIn = foundUser.registerIn.toString().slice(0, 15);

				done(null, user);
			}
		});
	}

  function getUserByNickname (nickname, done) {
  	models.User.findOne({ 'nickname': nickname }, done);
  }

  function getProfileById (userId, done) {
  	models.User.findById(userId
  	, '_id nickname email birthday avatar address rateHistories registerIn'
  	, function (err, foundUser) {
  		if (err) {
  			done(err, null);  
  		} else {
  			var user = {
  				id: foundUser._id,
  				nickname: foundUser.nickname,
  				email: foundUser.email,
  				birthday: foundUser.birthday || 'unknown',
  				avatar: foundUser.avatar,
  				address: foundUser.address || 'unknown',
  				registerIn: foundUser.registerIn,
  				rateHistories: foundUser.rateHistories
  			};
  			user.scoreObj = _.reduce(foundUser.rateHistories
				, function(memo, rateHistory) {
					if (memo.amount === 0) {
						return {
							score: rateHistory.score,
							amount: rateHistory.amount
						}
					} else {
  					return { 
  						score: ( ((rateHistory.score * rateHistory.amount) + (memo.score * memo.amount)) / (rateHistory.amount + memo.amount) ).toFixed(1),
  						amount: rateHistory.amount + memo.amount
  					}
  				}
				}, { score: 0, amount: 0 });

				if (user.scoreObj.score >= 2.8) {
					user.level = 'advanced';
				} else {
					if (user.scoreObj.score >= 1.2) {
						user.level = 'intermediate';
					} else {
						user.level = 'beginner';
					}
				}

				user.formatedBirthday = user.birthday.toString().slice(0, 15);

				user.formatedRegisterIn = user.registerIn.toString().slice(0, 24);

				done(null, user);
  		}
  	});
  }

	function getUsers (keyword, userId, done) {
		// nickname LIKE keyword
		var regExp = new RegExp(keyword, 'i');
		models.User.findById(userId, function (err, foundUser) {
			if (err) {
				done(err, null);
			} else {
				models.User.find({ nickname: regExp }, '_id nickname avatar registerIn currentStatus rateHistories'
				, function (err, users) {
					if (err) {
						done(err, null);
					} else {
						
						var result = [], isFriend;

						_(users).each(function (user) {
							isFriend = false;
							// don't recevie result is itself
							if (user._id.toString() !== userId.toString()) {
								// check that did users add friend?
								_(foundUser.friends).each(function (friend) {
									if (friend.userId.toString() === user._id.toString())
										isFriend = true;
								});

								var userDomain = {
									id: user._id,
									nickname: user.nickname,
									avatar: user.avatar,
									registerIn: user.registerIn,
									currentStatus: user.currentStatus,
									isFriend: isFriend
								}

								userDomain.scoreObj = _.reduce(user.rateHistories
								, function(memo, rateHistory) {
									if (memo.amount === 0) {
										return {
											score: rateHistory.score,
											amount: rateHistory.amount
										}
									} else {
				  					return { 
				  						score: ( ((rateHistory.score * rateHistory.amount) + (memo.score * memo.amount)) / (rateHistory.amount + memo.amount) ).toFixed(1),
				  						amount: rateHistory.amount + memo.amount
				  					}
				  				}
								}, { score: 0, amount: 0 });

								if (userDomain.scoreObj.score >= 2.8) {
									userDomain.level = 'advanced';
								} else {
									if (userDomain.scoreObj.score >= 1.2) {
										userDomain.level = 'intermediate';
									} else {
										userDomain.level = 'beginner';
									}
								}

								// push user into found list
								result.push(userDomain);
							}
							result = _(result).sortBy(function (user) {
								return user.scoreObj.score;
							}).reverse();
						});
						done(null, result);
					}
				});
			}
		});
	}

	function getUserId (email, encryptedPassword, done) {
		models.User.findOne({ email: email, password: encryptedPassword }
		, '_id', done);
	}

	function update (userId, avatarLink, birthday, address, done) {
		if (avatarLink !== '') {
			models.User.findByIdAndUpdate(userId
			, { $set: {
					avatar: avatarLink,
					birthday: birthday,
					address: address
				}}
			, function (err, founduser) {
				if (err) {
					done(err);
				} else {
					done(null, founduser);
				}
			});	
		} else {
			models.User.findByIdAndUpdate(userId
			, { $set: {
					birthday: birthday,
					address: address
				}}
			, function (err, founduser) {
				if (err) {
					done(err);
				} else {
					done(null, founduser);
				}
			});	
		}
	}

	function getAwesomePeople (done) {
		models.User.find({ }
		, '_id nickname avatar currentStatus registerIn rateHistories'
		, function (err, users) {
			var result = [];
			_(users).each(function (user) {
				var friendDomain = {
					id: user._id,
					nickname: user.nickname,
					avatar: user.avatar,
					currentStatus: user.currentStatus,
					registerIn: user.registerIn
				}
				
				friendDomain.scoreObj = _.reduce(user.rateHistories
				, function(memo, rateHistory) {
					if (memo.amount === 0) {
						return {
							score: rateHistory.score,
							amount: rateHistory.amount
						}
					} else {
						return { 
							score: ( ((rateHistory.score * rateHistory.amount) + (memo.score * memo.amount)) / (rateHistory.amount + memo.amount) ).toFixed(1),
							amount: rateHistory.amount + memo.amount
						}
					}
				}, { score: 0, amount: 0 });

				if (friendDomain.scoreObj.score >= 2.8) {
					friendDomain.level = 'intermediate';
				} else {
					if (friendDomain.scoreObj.score >= 1.2) {
						friendDomain.level = 'advance';
					} else {
						friendDomain.level = 'beginner';
					}
				}

				friendDomain.formatedRegisterIn = friendDomain.registerIn.toString().slice(0, 15);

				result.push(friendDomain);
			});
			done(null, result);
		});
	}

	return {
		register: register,
		getUsers: getUsers,
	    getUserByEmailPassword: getUserByEmailPassword,
	    getUserId: getUserId,
	    getProfileById: getProfileById,
	    getUserByNickname: getUserByNickname,
	    checkEmailExist: checkEmailExist,
	    update: update,
	    getShortProfile: getShortProfile,
	    getAwesomePeople: getAwesomePeople
	}
}