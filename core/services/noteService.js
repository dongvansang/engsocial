module.exports = function (mongoose, models) {
	var async = require('async'),
		_ = require('underscore'),
		monthName = [
			"Jan", "Feb", "Mar",
			"Apr", "May", "Jun",
			"Jul", "Aug", "Sep",
			"Oct", "Nov", "Dec"
		];

	/**
	 * get 'amount' of notes in 'page'
	 *
	 * @param {String} userId
	 * @param {Number} amount
	 * @param {Number} page
	 * @param {Function} done
	 */
	function getNotes (userId, amount, page, done) {
		models.User.findById(userId, function (err, user) {
			if (err) {
				done(err, null);
			} else {
				// length all notes
				var lengthNotes = user.notes.length,
					notes = user.notes,
					// amount of notes that can get
					amountNotes,
					// choose correct note then push into this result
					result = [];

				amountNotes = lengthNotes - ((page-1) * amount) < amount ? lengthNotes - ((page-1) * amount) : amount;

				notes = notes.splice(lengthNotes - (page-1) * amount - amountNotes, amountNotes);

				async.each(notes, iterator, function (err) {
					result = _(result).sortBy(function (note) {
						return note.createdIn;
					});
					result.reverse();
					done(err, result);
				});

				function iterator (noteId, done) {
					models.Post.findById(noteId
					, function (err, note) {
						if (err) {
							done(err);
						} else {
							var noteDomain = {
								id 				: note._id,
								content		: note.content,
								title 		: note.noteObj.title,
								refAt 		: note.noteObj.refAt,
								refOwner	: note.noteObj.refOwner,
								likes 		: note.likes || [],
								createdIn	: note.createdIn,
								comments	: note.noteObj.comments || []
							}
							result.push(noteDomain);
							done();
						}
					});
				}
			}
		});
	}

	/**
	 * Add a note by referencing from status
	 * @param {String} userId
	 * @param {String} refAt
	 * @param {Array} tags
	 * @param {Function} done
	 */
	function addNote (userId, title, refAt, refOwner, tags, done) {
		// find reference post
		models.Post.findById(refAt, cbFoundPost);
		function cbFoundPost (err, foundPost) {			
			if (err) { done(err, null); }
			else {
				// If user haven't yet added this
				if (foundPost.refBy.indexOf(userId) === -1) {
					var note = new models.Post();
					note.content = foundPost.content;
					note.owner = userId;
					note.typeObj = 'note';
					note.noteObj.title = title;
					note.noteObj.refAt = refAt;
					note.noteObj.refOwner = refOwner;
					note.save(cbSavedNote);
				} else {
					done('Opps! You noted this', null);
				}
			}

			/**
			 * When have a note, we will push into notes list
			 */
			function cbSavedNote (err, savedNote) {
				if (err) { done(err, null); }
				else {
					models.User.findByIdAndUpdate(userId
						, { $push: { notes: savedNote._id }}
						, cbFoundUser);
				}
				/**
				 * Push user that perform a note action into
				 * refBy list of post owner
				 */
				function cbFoundUser (err, foundUser) {
					if (err) { done(err, null); }
					else {
						// add this user to refBy list of owner post
						foundPost.refBy.push(userId);
						foundPost.save(cbSavePost);
					}
				}

				// return saved note
				function cbSavePost (err, savedPost) {
					if (err) { done(err, null); }
					else {
						done(null, savedNote);
					}
				}
			}// cbSavedNote
		}
	}

	return {
		getNotes: getNotes,
		addNote: addNote
	}
}