module.exports = function (mongoose, models) {
	var async = require('async'),
		statusService = require('./statusService')(mongoose, models),
		_ = require('underscore');

	function getNewsfeeds (userId, done) {
		models.User.findById(userId, function (err, foundUser) {
			var result = [];
			function iterator (friend, iteratorDone) {
				// find all user in friend list
				models.User.findById(friend.userId, function (err, user) {
					// get lastest status of each friend and push them into result
					var lastestStatusId = user.statuses[user.statuses.length-1];
					models.Post.findById(lastestStatusId
					, function (err, lastestStatus) {
						if (err) {
							done(err);
						} else {
							if (lastestStatus) {
								var statusDomain = statusService.toStatusDomain(user, lastestStatus);
								result.push(statusDomain);
							}
							iteratorDone();
						}
					});
				});
			}

			// order by friend.point
			// then find on top 9 friends that have high friend point
			var topFriends = _.sortBy(foundUser.friends
			, function (friend) {
				return friend.point;
			}).splice(0, 9);

			// insert me on top to get my newsfeed
			topFriends.splice(0, 0, { userId: userId });

			async.each(topFriends, iterator, function (err) {
				if (err) {
					done(err);
				} else {
					result = _(result).sortBy(function (status) {
						return status.createdIn;
					});
					done(null, result);
				}
			});
		});
	}

	return {
		getNewsfeeds: getNewsfeeds
	}
}