module.exports = function (mongoose, models) {
	var _ = require('underscore'),
		async = require('async');

	function addMessage(senderId, receiveId, msg, timestamp, done) {
		models.User.findById(senderId, 'chats'
		, function (err, foundUser) {
			if (err) {
				done(err, null);
			} else {
				if (foundUser) {
					var len = foundUser.chats.length,
						message = {
							senderId: senderId,
							message: msg,
							timestamp: timestamp
						},
						saveFlag = false;

					async.each(foundUser.chats, iterator, function (err) {
						if (err) {
							done(err, null);
						} else {
							if (!saveFlag) {
								// if saveFlag is false, didn't find conversation
								// need to add new one
								var conv = new models.Conversation();
								conv.usersId.push(senderId);
								conv.usersId.push(receiveId);
								conv.messages.push(message);
								conv.save(function (err, savedConv) {
									if (err) {
										done(err, null);
									} else {
										foundUser.chats.push({
											withUserId: receiveId,
											conversationId: savedConv._id
										});
										foundUser.save(function (err, savedSender) {
											if (err) {
												console.log(err);
											}
										});

										var chatOfReceiver = {
											withUserId: senderId,
											conversationId: savedConv._id
										};

										models.User.findByIdAndUpdate(receiveId
										, { $push: { chats: chatOfReceiver } }
										, function (err, savedReveiver) {
											if (err) {
												console.log(err);
											}
										});
									}
								});
							}
							done(null, 'OK');
						}
					});

					function iterator (chat, iteratorCb) {
						if (chat.withUserId.toString() === receiveId) {
							models.Conversation.findById(chat.conversationId
							, function (err, conv) {
								if (err) {
									iteratorCb(err);
								} else {
									conv.messages.push(message);
									conv.save(function (err, conv) {
										if (err) {
											iteratorCb(err);
										} else {
											saveFlag = true;
											iteratorCb();
										}
									});
								}
							});
						} else {
							iteratorCb();
						}
					}
				} else {
					done('Couldn\'t find ' + senderId, null);
				}
			}
		});
	}

	function getMessages (userId, chatUserId, done) {
		models.User.findById(userId
		, 'chats'
		, function (err, foundUser) {
			if (err) {
				done(err, null);
			} else {
				var messages = [];

				async.each(foundUser.chats, iterator, function (err) {
					done(err, messages);
				});

				function iterator (chat, iteratorCb) {
					if (chat.withUserId.toString() === chatUserId.toString()) {
						models.Conversation.findById(chat.conversationId
						, function (err, conv) {
							if (err) {
								iteratorCb(err);
							} else {
								messages = conv.messages;
								iteratorCb();	
							}
						});
					} else {
						iteratorCb();
					}
				}
			}
		});
	}

	return {
		addMessage: addMessage,
		getMessages: getMessages
	};
}