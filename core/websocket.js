module.exports = function (chatService, friendService, sio, sessionStore, sessionSecretString, app) {
	var Session = require('connect').session.Session,
		cookie = require('cookie'),
		utils = require('connect').utils;

	var _ = require('underscore');

	sio.configure(function () {
		sio.set('log level', 1);
		
		sio.set('authorization', function (data, callback) {
			if (typeof data.headers.cookie === 'undefined')
				return callback('Invalid session', false);

			var signedCookies = cookie.parse(data.headers.cookie),
			// get the signed cookies
				cookies = utils.parseSignedCookies(signedCookies, sessionSecretString);

			data.sessionId = cookies['express.sid'];
			data.sessionStore = sessionStore;
			data.sessionStore.get(data.sessionId, function(err, session) {
				if ( err || !session ) {
					return callback('Invalid session', false);
				} else {
					data.session = new Session(data, session);
					callback(null, true);
				}
			});
		});
	});

	sio.sockets.on('connection', function (socket) {
		var userId = socket.handshake.session.passport.user.id;
		
		app.usersOnline.push(userId);

		friendService.getFriendsId(userId, function(err, friends) {
			_(friends).each(function (friend) {
				_(sio.sockets.manager.handshaken)
				.each(function (handshake, socketId) {
					if (handshake.session.passport.user.id === friend.userId.toString()) {
						sio.sockets.sockets[socketId].emit('userOnline', userId);
					}
				});
			});
		});

		socket.on('message', function (msgObj) {
			var receiverId = msgObj.receiverId;
			// save to database
			chatService.addMessage(msgObj.senderId, msgObj.receiverId, msgObj.msgData.msg, msgObj.msgData.timestamp
			, function (err, message) {
				if (err) {
					console.log(err);
				} else {
					_(sio.sockets.manager.handshaken).each(function (handshake, socketId) {
						if (handshake.session.passport.user.id === receiverId) {
							sio.sockets.sockets[socketId].emit('receiveMsg', msgObj);
						}
					});
				}
			});
		});

		socket.on('disconnect', function () {
			var index = app.usersOnline.indexOf(userId);

			if (index !== -1) {
				// remove from online list
				app.usersOnline.splice(index, 1);
			}

			friendService.getFriendsId(userId, function(err, friends) {
				_(friends).each(function (friend) {
					_(sio.sockets.manager.handshaken)
					.each(function (handshake, socketId) {
						if (handshake.session.passport.user.id === friend.userId.toString()) {
							sio.sockets.sockets[socketId].emit('userOffline', userId);
						}
					});
				});
			});
		});
	});
}