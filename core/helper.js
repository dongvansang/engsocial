module.exports = function () {
  var crypto = require("crypto"),
    secretKey = "abcABC@123";
	var date = {
		/**
		 * Convert date object to human time
		 * 
		 * @param datetime [Date]. The date time need to convert
		 */
		dateConverter: function (datetime) {
			var currentDatetime = new Date(),
				subtract = (currentDatetime - datetime) / 3600000.0, // convert from milisecond to hour
				// In case, the unit is one, it will not have 's' suffix
				suffix;
			if (subtract < 1/60.0)
				subtract = "a few second ago";
			else {
				if (subtract >= 1/60.0 && subtract < 1 )
					subtract = Math.round(subtract * 60) + " minutes ago";
				else {
					if (subtract >= 1 && subtract <= 24) {
						if (subtract == 1) suffix = " hour ago";
						else suffix = " hours ago";
						subtract = Math.round(subtract) + suffix;
					} else {
						// if greater than 1 day and little or equal 30 days
						if (subtract > 24 && subtract <= 30 * 24) {
							if (Math.round(subtract / 24) == 1)
								suffix = " day ago";
							else suffix = " days ago";
							subtract = Math.round(subtract / 24) + suffix;
						}
					}
				}
			}
			return subtract;
		},

		convertObjectIdToDatetime: function (id) {
			var timestamp = id.substring(0, 8);
			return new Date(parseInt(timestamp, 16) * 1000); 
		}
	}

	var stringUtils = {
		sliceShort: function (string) {
			return string;
		}
	}
    
  var encryption = {
    encrypt: function (srcString) {
      var cipher = crypto.createCipher('AES-128-CBC', secretKey),
          crypted = cipher.update(srcString, 'utf8', 'hex');
      
      crypted += cipher.final('hex');
      return crypted;
    },
    
    decrypt: function (targetString) {
      var decipher = crypto.createDecipher('AES-128-CBC', secretKey),
          decrypted = decipher.update(targetString, 'hex', 'utf8');
          
      decrypted += decipher.final('utf8');
      return decrypted;
    }
  }

  var cookieToUserId = function (auth, userService, done) {
  	var decryptedAuth = encryption.decrypt(auth),
  		email = decryptedAuth.split(',')[0],
  		password = decryptedAuth.split(',')[1];

  	userService.getUserId(email, encryption.encrypt(password)
  	, function (err, user) {
  		if (err) {
  			done(err, null);
  		} else {
  			if (user === null)
  				done('Unauthentication', null);
  			else done(null, user._id);
  		}
  	});
  }

	return {
		date: date,
		stringUtils: stringUtils,
    encryption: encryption,
    cookieToUserId: cookieToUserId
	}	
}