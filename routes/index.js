
module.exports = function (app, services, passport) {

	var jade = require('jade'),
		fs = require('fs'),
		_ = require('underscore'),
		path = './views/includes/login-body.jade',
  	// form = require('connect-form'),
		compilerOptions = { filename: path },
		loginTemplateCompiler = jade.compile(fs.readFileSync(path), compilerOptions);
	
	app.get('/login', function (req, res) {
		if (req.user) {
			res.redirect('/');
		} else {
			res.render('login', { loginEmail: '', loginEmail: '', signupEmail: '', signupNickname: '' }); 
		}
	});

	app.post('/login'
	, passport.authenticate('local', { failureRedirect: '/login', failureFlash: true })
	, function(req, res) {
		// Authentication successful. Redirect home.
		res.cookie('loggedInUser', req.user._id.toString());
		res.cookie('nickname', req.user.nickname);
		res.cookie('avatar', req.user.avatar);
		res.redirect('/');
	});

	app.get('/logout', function (req, res) {
		req.logOut();
		res.redirect('/login');
	});
	
	app.post('/signup', function (req, res) {
		var email = req.param('email') || '',
			nickname = req.param('nickname') || '',
			password = req.param('password') || '',
			repassword = req.param('repassword') || '',
			checkError, emailError, nicknameError, passwordError, repasswordError;

		if (password !== '' && repassword !== '' && password !== repassword) {  
			repasswordError = "Re-password is not match";
			checkError = true;
		} else {
			if (password === '') {
				passwordError = "Password is require";
				checkError = true;
			}

			if (repassword === '') {
				repasswordError = "Re-password is require";
				checkError = true;
			}
		}

		if (nickname === '') {
			nicknameError = "Nickname is require";
			checkError = true;
		}

		if (email === '') {
			emailError = "Email is require";
			checkError = true;
			onCheckEmail(null, false);
		} else {
			var pattern = /^[a-zA-Z0-9_\.]+@[a-zA-Z]+?\.[a-zA-Z]{2,3}$/,
				regexp = new RegExp(pattern);
			if (!regexp.test(email)) {
				emailError = "Email is not correct format";
				checkError = true;
				onCheckEmail(null, false);
			} else {
				services.user.checkEmailExist(email, onCheckEmail);
			}
		}

		function onCheckEmail (err, isExist) {
			if (err) {
				res.send(500, err);
			} else {
				if (isExist) {
					emailError = "Email is exist, please use another email";
					checkError = true;
				}

				if (checkError) {
					var html = loginTemplateCompiler({ signupEmailError: emailError, signupPasswordError: passwordError, signupRepasswordError: repasswordError, signupNicknameError: nicknameError, loginEmail: '', signupEmail: email, signupNickname: nickname });
					res.send(500, html);
				} else {
					services.user.register(email, nickname, app.helper.encryption.encrypt(password)
					, function (err, user) {
						if (err) {
							html = loginTemplateCompiler({ globalError: { title: "Error", msg: err }, loginEmail: '', signupEmail: '', signupNickname: ''});
							res.send(500, html);
						} else {
							// Intiate a login session for 'user'
							req.logIn(user, function (err) {
								if (err) {
									res.send(500, err);
								} else {
									res.cookie('loggedInUser', user._id.toString());
									res.cookie('nickname', user.nickname);
									res.cookie('avatar', user.avatar);
									res.send(200);
								}
							});
							
						}
					});
				}
			}
		}
	});
	
	app.get('/', ensureAuthenticated
	, function (req, res) {
		res.render('index', { title: 'EngSocial network', loggedNickname: req.user.nickname, avatar: req.user.avatar });
	});

	app.get('/wall', ensureAuthenticated
	, function (req, res) {
		res.render('wall', { title: 'Wall page', loggedNickname: req.user.nickname, avatar: req.user.avatar });
	});

	app.get('/personal-note', ensureAuthenticated
	, function (req, res) {
		res.render('personal-note', { title: 'Personal note', loggedNickname: req.user.nickname, avatar: req.user.avatar });
	});

	app.get('/profile', ensureAuthenticated, function (req, res) {
		services.user.getProfileById(req.user._id
		, function (err, foundUser) {
			res.render('profile', { title: 'Profile'
			, loggedNickname: req.user.nickname
			, avatar: req.user.avatar
			, user: foundUser });
		});
	});

	app.post('/profile', function (req, res) {
  	var nickname = req.user.nickname,
	  	birthday = req.param('birthday'),
	  	address = req.param('address'),
	  	avatarLink = '';

  	if (req.files.avatar.filename !== '') {
  		var filename = req.files.avatar.filename,
  			newPath = app.rootDir
		  		+ "/public/img/avatars/"
		  		+ nickname + "-"
		  		+ filename;

			fs.readFile(req.files.avatar.path, function (err, data) {
			  fs.writeFile(newPath, data, function (err) {
			  	if (err) {
			  		res.send(500, err);
			  	} else {
			  		avatarLink = '/img/avatars/' + nickname + '-' + filename;
			  		services.user.update(req.user._id
			  		, avatarLink
			  		, birthday
			  		, address
			  		, function (err, user) {
			  			if (err) {
			  				res.send(500, err);
			  			}	else {
			  				res.cookie('avatar', user.avatar);
			  				res.redirect('/profile');
			  			}
			  		});
			  	}
		  	});
			});
		} else {
			services.user.update(req.user._id
  		, avatarLink
  		, birthday
  		, address
  		, function (err, done) {
  			if (err) {
  				res.send(500, err);
  			}	else {
  				res.redirect('/profile');
  			}
  		});
		}
	});

	app.get('/api/:nickname/profile', function (req, res) {
		var nickname = req.params.nickname;

		services.user.getShortProfile(nickname
		, function (err, foundUser) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, foundUser);    
			}
		});
	});

	app.post('/like', function (req, res) {
		var loggedInUser = req.user._id,
			postId = req.body.postId;
		services.post.like(loggedInUser, postId, function (err, post) {
			if (err) { console.log(err); }
			else res.send(200);
		});
	});

	app.post('/unlike', function (req, res) {
		var loggedInUser = req.user._id,
			postId = req.body.postId;
		services.post.unlike(loggedInUser, postId, function (err, post) {
			if (err) { console.log(err); }
			else res.send(200);
		});
	});

	app.get('/who-like-this/:id', function (req, res) {
		var postId = req.params.id; 
		services.post.getLikeUsers(postId, function (err, likeUsers) {
			if (err) { console.log(err); }
			else {
				var html = app.helper.render.likeUsersRender({ likeUsers: likeUsers });
				res.send(200, html);
			}
		});
	});

	app.post('/rating', function (req, res) {
		var loggedInUser = req.user._id,
			postId = req.param('postId'),
			score = req.param('score');
		services.post.rate(loggedInUser, postId, score
		, function (err, rates) {
			if (err) {
				res.send(500, err);
			}
			else {
				res.send(200, rates);
			}
		});
	});

	app.get('/awesome-people', function (req, res) {
		services.user.getAwesomePeople(function (err, users) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, users);
			}
		});
	});

	app.get('/:nickname', ensureAuthenticated, function (req, res) {
		var loggedNickname = req.user.nickname,
			nickname = req.params.nickname;

		services.user.getUserByNickname(nickname, function (err, foundUser) {
			if (err) {
				// TODO handle error
			} else {
				if (foundUser) {
					var isFriend = false;
					_(foundUser.friends).each(function (friend) {
						if (friend.userId.toString() === req.user._id.toString()) {
							isFriend = true;
						}
					});

					res.render('sibling-wall', { loggedNickname: loggedNickname, title: nickname + '\'s wall', userId: foundUser._id,  nickname: nickname, avatar: req.user.avatar, isFriend: isFriend });    
				} else {
					res.send(500, 'Couldn\'t find url');
				}
			}
			
		});
	});

	// Simple route middleware to ensure user is authenticated.
	function ensureAuthenticated(req, res, next) {
		if (req.isAuthenticated()) { return next(); }
		res.redirect('/login')
	}
}