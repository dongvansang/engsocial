module.exports = function (app, services) {

	/**
	 * Add a note
	 */
	app.post('/api/note', function (req, res) {
		var loggedInUser = req.user._id,
			title = req.param('title'),
			refAt = req.param('refAt'),
			refOwner = req.param('refOwner'),
			tags = req.param('tags');

		tags = tags.split(',');
		services.note.addNote(loggedInUser, title, refAt, refOwner, tags
		, function (err, note) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(201, note);	
			}
		});
	});

	/**
	 * Personal note page
	 */
	app.get('/api/notes', function (req, res) {
		var userId, page, amount;
			userId = req.query.userId;
			page = req.query.page;
			amount = req.query.amount;

		services.note.getNotes(userId, amount, page
		, function (err, notes) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, notes);
			}
		});
	});
}