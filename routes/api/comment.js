module.exports = function (app, services) {

	app.post('/api/comment', function (req, res) {
		var loggedInUser = req.user._id;

		// if isStatus is true, add comment to statusObj, unless noteObj
		services.comment.addComment(loggedInUser, req.body.content, req.body.parent, req.body.isStatus //true: is a status
		, function (err, comment) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(201, comment);	
			}
		});
	});

	// ex: get top 5 comment of a status
	app.get('/api/comments', function (req, res) {
		var postId = req.query.postId,
			amount = req.query.amount,
			page = req.query.page;

		services.comment.getComments(postId, amount, page, function (err, comments) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, comments);
			}
		});
	});
}