module.exports = function (app, services) {

	app.get('/api/newsfeeds', function (req, res) {
		var loggedInUser = req.user._id;
		
		services.newsfeed.getNewsfeeds(loggedInUser
		, function (err, newsfeeds) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, newsfeeds);
			}
		});
	});
}