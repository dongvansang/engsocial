module.exports = function (app, services) {

	var _ = require('underscore');

	app.get('/api/friends', function (req, res) {
		var userId = req.param('userId'),
			amount = req.param('amount');

		if (userId === 'me')
			userId = req.user._id;
			
		services.friend.getFriends(userId, amount
		, function (err, friends) {			
			if (err) {
				res.send(500, err);
			} else {
				_(friends).each(function (friend) {
					// check user's contact status
					friend.isOnline = (app.usersOnline.indexOf(friend.id.toString()) !== -1);
				});

				_(friends).sortBy(function (friend) {
					return friend.isOnline;
				});

				res.send(200, friends);
			}
		});
	});

	app.post('/api/friend/:userId', function (req, res) {
		var loggedInUser = req.user._id,
			userId = req.param('userId');

		services.friend.addFriend(loggedInUser, userId, 0, function (err, message) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, message);
			}
		});
	});

	app.del('/api/friend/:userId', function (req, res) {
		var loggedInUser = req.user._id,
			userId = req.param('userId');
			
		services.friend.unFriend(loggedInUser, userId
		, function (err, message) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, message);
			}
		});
	});

	app.get('/friend-details', function (req, res) {
		var userId = req.body.userId
			, html = app.helper.render.friendCardDetailsRender();
		res.send(200, html);
	});
}