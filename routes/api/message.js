module.exports = function (app, services) {
	app.get('/api/messages', function (req, res) {
		if (typeof req.user === 'undefined') {
			res.send(401);
		} else {
			var chatWithUser = req.param('chatWithUser');
			services.chat.getMessages(req.user._id, chatWithUser
			, function (err, messages) {
				if (err) {
					res.send(500, err);
				} else {
					res.send(200, messages)
				}
			})
		};
	});
}