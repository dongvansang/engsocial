module.exports = function (app, services) {

	app.get('/rates', function (req, res) {
		res.render('rate-history', { title: 'Rate history' });
	});
}