module.exports = function (app, services) {
	
	app.del('/api/post/:id', function (req, res) {
		var loggedInUser = req.user._id,
			postId = req.params.id;
			
		services.post.deletePost(loggedInUser, postId, function (err, deletedUser) {
			if (err) { send(500, err) }
			else {
				res.send(200, deletedUser);
			}
		});
	});
}