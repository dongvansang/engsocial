module.exports = function (app, services) {

	app.post('/api/edit', function (req, res) {
		var loggedInUser = req.user._id,
			postId = req.param('postId'),
			editText = req.param('editText'),
			editTo = req.param('editTo'),
			content = req.param('content');

		services.edit.addEdit(loggedInUser, content, postId, editText, editTo
		, function (err, savedEdit) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(201, savedEdit);
			}
		});
	});

	app.get('/api/edits', function (req, res) {
		var postId = req.param('postId'),
			amount = req.param('amount'),
			page = req.param('page');

		services.edit.getEdits(postId, amount, page
		, function (err, edits) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, edits);
			}
		});
	});
}