module.exports = function (app, services) {
	// add a status
	app.post('/api/status', function (req, res) {
		var loggedInUser = req.user._id,
			content = req.body.content;
		services.status.addStatus(loggedInUser, content
		, function (err, statusDomain) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(201, statusDomain);
			}
		});
	});

	app.get('/api/statuses', function (req, res) {
		var userId = req.param('userId'),
			amount = req.param('amount'),
			page = req.param('page');
			
		if (userId === 'me')
			userId = req.user._id;
		
		services.status.getStatuses(userId, amount, page
		, function (err, statuses) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, statuses);
			}
		});
	});
}