module.exports = function (app, services) {

	app.get('/api/searching', function (req, res) {
		var loggedInUser = req.user._id,
			keyword = req.query.keyword;

		services.user.getUsers(keyword, loggedInUser, function (err, users) {
			if (err) {
				res.send(500, err);
			} else {
				res.send(200, users);
			}
		});
	});
}