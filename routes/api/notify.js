module.exports = function (app, services) {

	var getUntouchNotifies = function (req, res) {
		app.helper.cookieToUserId(req.cookies.auth
		, services.user, onValidUser);

		function onValidUser (err, loggedInUser) {
			var newestNotify = req.params.id || null;
			services.user.getUntouchNotifies(loggedInUser, newestNotify
				, function (notifies) {
				var html = "";
				for (var i = 0; i < notifies.length; i++) {
					// compile to html code
					notifies[i].createdInConverted = helper.date
						.dateConverter(notifies[i].createdIn);
					html = html + render.notifyRender(notifies[i]);
				};
				res.send(html);
			});
		}
	}

	return {
		getUntouchNotifies: getUntouchNotifies
	}
}