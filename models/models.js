module.exports = function (mongoose) {
	var schemas = require('./schemas')(mongoose);

	return {
		Post: mongoose.model('Post', schemas.PostSchema),
		Notify: mongoose.model('Notify', schemas.NotifySchema),
		User: mongoose.model('User', schemas.UserSchema),
		Conversation: mongoose.model('Conversation', schemas.ConversationSchema)
	}
}