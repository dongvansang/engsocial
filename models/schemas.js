/**
 * Define schemas for all models
 */
module.exports = function (mongoose) {

	var PostSchema = new mongoose.Schema({
		content: { type: String },
		owner: { type: mongoose.Schema.Types.ObjectId },
		likes: [{ type: mongoose.Schema.Types.ObjectId }],
		refBy: [{ type: mongoose.Schema.Types.ObjectId }],
		createdIn: { type: Date, default: Date.now },
		typeObj: { type: String }, // is 'status', 'note', 'comment' or 'edit'
		statusObj: {
			comments: [{ type: mongoose.Schema.Types.ObjectId }],
			rates: [{
				userId: { type: mongoose.Schema.Types.ObjectId },
				score: { type: Number },
				lastModified: { type: Date }
			}],
			followers: [{ type: mongoose.Schema.Types.ObjectId }],
			edits: [{ type: mongoose.Schema.Types.ObjectId }]
		},
		commentObj: {
			parent: { type: mongoose.Schema.Types.ObjectId },
			rates: [{
				userId: { type: mongoose.Schema.Types.ObjectId },
				score: { type: Number },
				lastModified: { type: Date }
			}]
		},
		editObj: {
			parent: { type: mongoose.Schema.Types.ObjectId },
			editText: { type: String },
			editTo: { type: String },
			rates: [{
				userId: { type: mongoose.Schema.Types.ObjectId },
				score: { type: Number },
				lastModified: { type: Date }
			}],
		},
		noteObj: {
			title: { type: String },
			comments: [{ type: mongoose.Schema.Types.ObjectId }],
			refAt: { type: mongoose.Schema.Types.ObjectId },
			refOwner: { type: String },
			catelogies: [{ type: String }]
		}
	});

	var ConversationSchema = new mongoose.Schema({
		usersId: [{ type: mongoose.Schema.Types.ObjectId }],
		messages: [{
			senderId: { type: mongoose.Schema.Types.ObjectId },
			message: { type: String },
			timestamp: { type: Date }
		}]
	});

	var UserSchema = new mongoose.Schema({
		email: { type: String, unique: true },
		nickname: { type: String, unique: true },
		password: { type: String },
		birthday: { type: Date },
		avatar: { type: String, default: '/img/default-avatar.jpg' },
		address: { type: String },
		rateHistories: [{
			postId: { type: mongoose.Schema.Types.ObjectId },
			score: { type: Number },
			amount: { type: Number },
			lastModified: { type: Date }
		}],
		friends: [{
			userId: { type: mongoose.Schema.Types.ObjectId },
			point: { type: Number },
			createdIn: { type: Date, default: Date.now }
		}],
		currentStatus: { type: String, default: '' },
		statuses: [{ type: mongoose.Schema.Types.ObjectId }],
		notes: [{ type: mongoose.Schema.Types.ObjectId }],
		notifies: [{ type: mongoose.Schema.Types.ObjectId }],
		newsFeeds: [{ type: mongoose.Schema.Types.ObjectId }],
		chats: [{
			withUserId: { type: mongoose.Schema.Types.ObjectId },
			conversationId: { type: mongoose.Schema.Types.ObjectId }
		}],
		registerIn: { type: Date, default: Date.now },
		lastModified: { type: Date }
	});

	var NotifySchema = new mongoose.Schema({
		owner: {
			nickname: { type: String },
			avatar: { type: String }
		},
		message: { type: String },
		link: { type: String },
		isTouch: { type: Boolean, default: false },
		createdIn: { type: Date, default: Date.now }
	});

	NotifySchema.methods.touch = function (notifyId, done) {
		this.model('Notify').findByIdAndUpdate(notifyId
		, { $set: { isTouch: true } }
		, done);
	}
	
	return {
		NotifySchema 			: NotifySchema,
		PostSchema 				: PostSchema,
		UserSchema 				: UserSchema,
		ConversationSchema: ConversationSchema
	}
}