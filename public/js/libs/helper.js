var date = {
	/**
	 * Convert date object to human time
	 * 
	 * @param datetime [Date]. The date time need to convert
	 */
	dateConverter: function (datetime) {
		var currentDatetime = new Date(),
			subtract = (currentDatetime - datetime) / 3600000.0, // convert from milisecond to hour
			// In case, the unit is one, it will not have 's' suffix
			suffix;
		if (subtract < 1/60.0)
			subtract = "a few second ago";
		else {
			if (subtract >= 1/60.0 && subtract < 1 )
				subtract = Math.round(subtract * 60) + " minutes ago";
			else {
				if (subtract >= 1 && subtract <= 24) {
					if (subtract == 1) suffix = " hour ago";
					else suffix = " hours ago";
					subtract = Math.round(subtract) + suffix;
				} else {
					// if greater than 1 day and little or equal 30 days
					if (subtract > 24 && subtract <= 30 * 24) {
						if (Math.round(subtract / 24) == 1)
							suffix = " day ago";
						else suffix = " days ago";
						subtract = Math.round(subtract / 24) + suffix;
					}
				}
			}
		}
		return subtract;
	},

	convertObjectIdToDatetime: function (id) {
		var timestamp = id.substring(0, 8);
		return new Date(parseInt(timestamp, 16) * 1000); 
	}
}