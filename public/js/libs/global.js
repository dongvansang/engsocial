
var createCookie = function (name, value) {
    // var cookie = [name, '=', JSON.stringify(value), '; domain=.', window.location.host.toString(), '; path=/;'].join('');
    document.cookie = name + "=" + JSON.stringify(value);
}

var readCookie = function (name) {
    var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
    try {
	    result && (result = JSON.parse(result[1]));
	  } catch(SyntaxError) {
	  	result = result[1];
	  }
  return result;
}

var deleteCookie = function (name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
}

var loggedInUser = '';