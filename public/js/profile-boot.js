var app = app || {};

define(['views/components/friends', 'views/components/search', 'views/layouts/chat', 'models/friend', 'Backbone', 'jQuery.autosize', 'jQuery.rateit', 'bootstrap', 'jQuery.slimScroll']
, function (FriendsView, SearchView, ChatView, Friend) {
	
	var friendsView = new FriendsView();
	var searchView = new SearchView();
	var chatView = new ChatView();

	$.ajax({
		type: 'GET',
		url: '/api/friends',
		data: {
			userId: 'me'
		},
		statusCode: {
			200: function (friends) {
				_(friends).each(function (friend) {
					friendsView.collection.add(new Friend(friend));
				});
			}
		}
	});
});