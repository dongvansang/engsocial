var app = app || {};

define(['views/layouts/personal-note', 'views/components/search', 'views/layouts/chat', 'views/components/note', 'views/components/notes', 'views/components/friends', 'models/friend', 'Backbone', 'jQuery.autosize', 'jQuery.rateit', 'bootstrap']
, function (PersonalNoteView, SearchView, ChatView, NoteView, NotesView, FriendsView, Friend) {
	
	var personalNoteView = new PersonalNoteView();
	var searchView = new SearchView();
	var chatView = new ChatView();
	var friendsView = new FriendsView();

	$.ajax({
		type: 'GET',
		url: '/awesome-people',
		statusCode: {
			200: function (users) {
				_(users).each(function (user) {
					friendsView.collection.add(new Friend(user));
				});
			}
		}
	});

});