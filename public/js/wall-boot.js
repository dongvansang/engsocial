var app = app || {};

define(['views/layouts/wall', 'views/components/search', 'views/layouts/chat', 'views/components/chat-docks', 'views/components/friends', 'models/friend', 'Backbone', 'jQuery.autosize', 'jQuery.rateit', 'bootstrap', 'jQuery.slimScroll']
 	, function (WallView, SearchView, ChatView, ChatDocksView, FriendsView, Friend) {

	var wallView = new WallView();
	var searchView = new SearchView();
	var chatView = new ChatView();
	var chatDocksView = new ChatDocksView();
	var friendsView = new FriendsView();

	$.ajax({
		type: 'GET',
		url: '/awesome-people',
		statusCode: {
			200: function (users) {
				_(users).each(function (user) {
					friendsView.collection.add(new Friend(user));
				});
			}
		}
	});
});