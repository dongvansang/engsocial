define(['models/note'], function (Note) {
	var Notes = Backbone.Collection.extend({
		model: Note
	});

	return Notes;
});