define(['models/chat-dock'], function (ChatDock) {
	var ChatDocks;
	ChatDocks = Backbone.Collection.extend({
		model: ChatDock
	});

	return ChatDocks;
});