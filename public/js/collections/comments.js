define(['models/comment', 'Backbone'], function (Comment) {
	var Comments = Backbone.Collection.extend({
		model: Comment,

		append: function (comment) {
			this.add(comment);
			this.trigger('append', comment);
		},

		prepend: function (comment) {
			this.add(comment);
			this.trigger('prepend', comment);
		},
	});

	return Comments;
});