define(['models/status', 'Backbone'], function (Status) {
	var Statuses = Backbone.Collection.extend({
		model: Status
	});

	return Statuses;
});