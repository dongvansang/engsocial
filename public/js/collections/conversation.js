define(['models/message', 'Backbone']
, function (Message) {
	var Conversation;
	Conversation = Backbone.Collection.extend({
		model: Message
	});

	return Conversation;
});