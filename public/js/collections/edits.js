define(['models/edit', 'Backbone'], function (Edit) {
	var Edits = Backbone.Collection.extend({
		model: Edit
	});

	return Edits;
});