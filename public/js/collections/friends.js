define(['models/friend', 'Backbone'], function (Friend) {
	var Friends = Backbone.Collection.extend({
		model: Friend
	});

	return Friends;
});