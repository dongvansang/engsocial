define(['models/chat-user', 'Backbone'], function (ChatUser) {
	var ChatUsers;
	ChatUsers = Backbone.Collection.extend({
		model: ChatUser,

		url: '/api/friends',

		/**
		 * find chat user by nickname 
		 *
		 * @param {String} keyword
		 * @return {ChatUsers}
		 */
		byNickname: function (keyword) {
			var matchResults = this.filter(function (chatUser) {
				var regExp = new RegExp(keyword, 'i');	
				return regExp.test(chatUser.get('nickname'));
			});
			return new ChatUsers(matchResults);	
		}
	});

	return ChatUsers;
});