define(['models/search-item', 'Backbone']
, function (SearchItem) {
	var SearchItems = Backbone.Collection.extend({
		model: SearchItem
	});

	return SearchItems;
});