var app = app || {};

define(['views/layouts/sibling-wall', 'views/components/search', 'views/layouts/chat', 'views/components/friends', 'models/friend', 'Backbone', 'jQuery.autosize', 'jQuery.rateit', 'bootstrap', 'jQuery.slimScroll']
, function (SiblingView, SearchView, ChatView, FriendsView, Friend) {

	var siblingView = new SiblingView();
	var searchView = new SearchView();
	var chatView = new ChatView();
	var friendsView = new FriendsView();

	$.ajax({
		type: 'GET',
		url: '/awesome-people',
		statusCode: {
			200: function (users) {
				_(users).each(function (user) {
					friendsView.collection.add(new Friend(user));
				});
			}
		}
	});
});