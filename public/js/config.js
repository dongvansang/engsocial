require.config({
	paths: {
		'socketIO': '/socket.io/socket.io',
		'jQuery': '/js/libs/jquery-1.8.3',
		'jQuery.autosize': '/js/libs/jquery.autosize',
		'jQuery.rateit': '/js/libs/jquery.rateit',
		'jQuery.draggable': '/js/libs/jquery-ui-1.10.2.draggable',
		'jQuery.slimScroll': '/js/libs/jquery.slimscroll',
		'bootstrap': '/js/libs/bootstrap',
		'underscore': '/js/libs/underscore',
		'Backbone': '/js/libs/backbone',
		'text': '/js/libs/text',
		'templates': '../templates'
	},

	// definition dependencies
	shim: {
		'bootstrap': ['jQuery'],
		'jQuery.autosize': ['jQuery'],
		'jQuery.rateit': ['jQuery'],
		'jQuery.draggable': ['jQuery'],
		'jQuery.slimScroll': ['jQuery.draggable'],
		'Backbone': ['underscore', 'jQuery']
	}
});

loggedInUser = readCookie('loggedInUser');
avatar = readCookie('avatar');

require(['text!templates/profile.html', 'jQuery', 'jQuery.autosize', 'bootstrap', 'underscore', 'socketIO']
, function (profileTemplate) {

	// update live timestamp
	setInterval(function () {
		var $liveTimestamps = $('.live-timestamp');
		_($liveTimestamps).each(function (liveTimestamp) {
			var $liveTimestamp = $(liveTimestamp),
				createdIn =  new Date($liveTimestamp.data('timestamp')),
				updatedLive = date.dateConverter(createdIn);
			$liveTimestamp.html(updatedLive);
		});
	}, 1000);

	var popoverTimeout, hoverTimeout;
	function delay (func) {
		popoverTimeout = setTimeout(func, 300);
	}

	$('#dashboard-menu > [data-toggle="tooltip"]').tooltip();

	var profTempCompiler = _.template(profileTemplate);
	
	$(document).on('mouseenter', '.nickname', function (e) {
		var $nickname = $(e.currentTarget);

		clearTimeout(hoverTimeout);

		hoverTimeout = setTimeout(function () {
			clearTimeout(popoverTimeout);
			$('.popover').detach();
			$.ajax({
				type: 'get',
				url: '/api/' + $nickname.text().trim() + '/profile',
				statusCode: {
					200: onSuccessCb,
					500: onErrorCb
				}
			});

			function onSuccessCb (user) {
				$nickname.popover({
					animation: false,
					placement: function (context, source) {
						if (typeof $nickname.data('placement') !== 'undefined')
							return $nickname.data('placement');

						var position = $(source).position(),
							windowWidth = $(window).width();

						if (position.top < 100)
							return "bottom";

						if (windowWidth - position.left < 500 )
							return "left";

						return "right";
					},
					trigger: 'manual',
					html: true,
					content: profTempCompiler(user),
				}).popover('show');	

				$nickname.siblings('.popover').find('.rateit').rateit();
			}

			function onErrorCb (err) {
				console.log(err);
				return;
			}
		}, 500);
	});

	$(document).on('mouseleave', '.nickname', function (e) {
		var $nickname = $(e.currentTarget);
		clearTimeout(hoverTimeout);
		delay(function () {
			$nickname.popover('hide');
		});

		$('.popover').mouseenter(function () {
			clearTimeout(popoverTimeout);
		}).mouseleave(function () {
			delay(function () {
				$nickname.popover('hide');
			});
		});
	});
});


