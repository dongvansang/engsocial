define(['collections/friends', 'views/components/friend']
, function (Friends, FriendView) {
	var FriendsView = Backbone.View.extend({
		el: '#list-friend',

		initialize: function () {
			_.bindAll(this, 'render', 'appendFriend');
			this.collection = new Friends();
			this.collection.bind('add', this.appendFriend);

			// render immediate when init
			this.render();
		},

		render: function () {
			this.$el.html('');
		},

		appendFriend: function (friend) {
			var friendView = new FriendView({
				model: friend
			});
			var $friendHtml = friendView.render().$el;
			$friendHtml.hide();
			this.$el.append($friendHtml);
			$friendHtml.fadeIn('fast');
		}
	});

	return FriendsView;
});