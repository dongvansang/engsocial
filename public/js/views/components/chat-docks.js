define(['views/components/chat-dock', 'collections/chat-docks', 'Backbone']
, function (ChatDockView, ChatDocks) {

	var ChatDocksView;
	ChatDocksView = Backbone.View.extend({
		el: '.chat-dock-wrapper',

		initialize: function () {
			_.bindAll(this, 'render', 'addChatDock', 'highlight');
			this.collection = new ChatDocks();
			this.collection.bind('add', this.addChatDock);
			this.render();
		},

		render: function () {
			this.$el.html('');
		},

		addChatDock: function (chatDock) {
			var chatDockView = new ChatDockView({
				model: chatDock
			});
			this.$el.append(chatDockView.render().$el);
			this.$el.find('.chat-dock' + "#" + chatDock.id).find('.input-msg-txt').focus();
		},

		highlight: function (userId) {
			var $chatDock = this.$('#' + userId);
			$chatDock.animate({
				'border': '3px solid #F00'
			}, 'fast');
		}
	});

	return ChatDocksView;
});