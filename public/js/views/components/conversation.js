define(['collections/conversation', 'views/components/message', 'models/message', 'Backbone']
, function (Conversation, MessageView, Message) {
	var ConversationView;
	ConversationView = Backbone.View.extend({
		tagName: 'ul',
		className: 'conversation',

		initialize: function () {
			_.bindAll(this, 'render', 'addMessage');
			this.collection.bind('add', this.addMessage);
		},

		render: function () {
			var self = this;
			this.$el.html('');
			_(this.collection.models).each(function (messageModel) {
				self.addMessage(messageModel);
			});
			return this;
		},

		addMessage: function (message) {
			var messageView = new MessageView({
				model: message
			});
			this.$el.append(messageView.render().$el);

			var $scroll = this.$el.parents('.scrollable'),
				scrollTo_val = $scroll.prop('scrollHeight') + 'px';

			$scroll.slimScroll({ scrollTo: scrollTo_val });
		}
	});

	return ConversationView;
});