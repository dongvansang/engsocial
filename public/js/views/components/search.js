define(['text!templates/search.html', 'views/components/search-items', 'models/search-item', 'Backbone']
, function (searchTemplate, SearchItemsView, SearchItem) {

	var keyPressTimeout;
	function delay (func) {
		keyPressTimeout = setTimeout(func, 300);
	}

	var SearchView = Backbone.View.extend({
		el: '.search',

		events: {
			'keyup #search-txt': 'search',
			// lose focus
			'blur .search-wrapper': 'hideSearchResult',
			'click #clear-search': 'resetSearchResult'
		},

		template: _.template(searchTemplate),

		initialize: function () {
			_.bindAll(this, 'search');
			this.render();
			this.searchItemsView = new SearchItemsView();
		},

		render: function () {
			this.$el.html(this.template);
		},

		search: function (e) {
			// prevent UP DOWN LEFT RIGHT key
			if (e.keyCode >= 37 && e.keyCode <=40) return;
			// if ESCAPSE key
			if (e.keyCode === 27) this.resetSearchResult();

			var self = this,
			$searchTxt = $(e.currentTarget);
			
			// delete previous schedule 
			clearTimeout(keyPressTimeout);

			// schedule a new one
			delay(performSearch);
			function performSearch () {
				self.searchItemsView.collection.reset();
				if ($searchTxt.val() === '') {
					self.resetSearchResult();
					return;
				}

				$('.search-results-wrapper').removeClass('hide');
				$.ajax({
					type: 'GET',
					url: '/api/searching',
					data: {
						keyword: $searchTxt.val()
					},
					success: function (data) {
						if (data.length === 0) {
							// if we have a no result notify, don't append
							if (self.$('#search-results > .not-found').length === 0) {
								self.$('#search-results').append('<div class="not-found">Not found</div>');
							}
						} else {
							self.$('.not-found').remove();
							_(data).each(function (user) {
								self.searchItemsView.collection
								.add(new SearchItem(user));
							});
						}
					}
				});	
			}
			
		},

		resetSearchResult: function () {
			this.$('#search-txt').val('');
			this.searchItemsView.collection.reset();
			this.$('.search-results-wrapper').slideDown('fast', function () {
					$self = $(this);
					setTimeout(function () {
						// add style('') to fix bug that automic add a display: block in style
						$self.addClass('hide').css('style', '');
					}, 500);
			});
		}
	});
	return SearchView;
});