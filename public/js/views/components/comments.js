define(['collections/comments', 'views/components/comment', 'Backbone']
, function (Comments, CommentView) {

	var CommentsView;
	CommentsView = Backbone.View.extend({
		tagName: 'ul',
		id: 'list-comment',

		initialize: function () {
			// that functions want to use this object in them
			_.bindAll(this, 'prependComment', 'appendComment');

			this.collection = new Comments();
			this.collection.bind('append', this.appendComment);
			this.collection.bind('prepend', this.prependComment);
		},

		render: function () {
			this.$el.html('');
			return this;
		},

		appendComment: function (comment) {
			var commentView = new CommentView({
				model: comment
			});

			// append new comment to #list-comment
			var $html = commentView.render().$el;
			$html.hide();
			this.$el.append($html);
			$html.fadeIn('slow');
		},

		prependComment: function (comment) {
			var commentView = new CommentView({
				model: comment
			});

			// append new comment to #list-comment
			var $html = commentView.render().$el;
			$html.hide();
			this.$el.prepend($html);
			$html.fadeIn('slow');
		}
	});

	return CommentsView;
});