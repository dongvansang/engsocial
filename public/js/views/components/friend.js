define(['text!templates/friend.html', 'Backbone'], function (friendTemplate) {
	var FriendView = Backbone.View.extend({
		tagName: 'li',

		template: _.template(friendTemplate),

		events: {
			'click .add-friend-btn': 'addFriend'
		},

		initialize: function () {
			_.bindAll(this, 'render');
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.attr('id', this.model.id);
			this.$('.rateit').rateit();
			
			return this;
		},

		addFriend: function () {
			var userId = this.model.id,
				$addFriendBtn = $(e.currentTarget);
				
			if ($addFriendBtn.text().trim() === 'Unfriend') {
				$addFriendBtn.text('Add friend');
				$.ajax({
					type: 'DELETE',
					url: '/api/friend/' + userId
				});
			} else {
				$addFriendBtn.text('Unfriend');
				$.ajax({
					type: 'POST',
					url: '/api/friend/' + userId
				});
			}
		}
	});

	return FriendView;
});