define(['models/note', 'views/components/note', 'collections/notes']
, function (Note, NoteView, Notes, NotesView) {
	var NotesView = Backbone.View.extend({
		tagName: 'ul',
		id: 'list-post',
		

		initialize: function () {
			_.bindAll(this, 'render', 'addNote');
			this.collection = new Notes();
			this.collection.bind('add', this.addNote);
		},

		render: function () {
			this.$el.html('');
			return this;
		},

		loadNotes: function (amount, page) {
			var self = this;

			$.ajax({
				type: 'GET',
				url: '/api/notes',
				data: {
					userId: readCookie('loggedInUser'),
					amount: amount,
					page: page
				},
				statusCode: {
					200: function (notes) {
						_(notes).each(function (note) {
							// add a new Friend model
							var noteModel = new Note(note);
							self.collection.add(noteModel);
						});
					}
				}
			});
		},

		addNote: function (note) {
			var noteView = new NoteView({
				model: note
			});

			var $noteHtml = noteView.render().$el;
			$noteHtml.hide();
			this.$el.append($noteHtml);
			$noteHtml.fadeIn('slow');

			noteView.loadComments(5, 1);
		}
	});

	return NotesView;
});