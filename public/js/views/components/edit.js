define(['text!templates/edit.html', 'views/components/post', 'Backbone']
, function (editTempalte, PostView) {

	var EditView;
	EditView = PostView.extend({
		tagName: 'li',
		
		events: {
			'mouseenter .edit-text': 'mouseEnterEditText',
      'mouseleave .edit-text': 'mouseLeaveEditText',
      'mouseenter .edit-to': 'mouseEnterEditTo',
      'click .edit-to': 'acceptEdition'
		},

		initialize: function () {
			this.events = _.extend({}, PostView.prototype.events, this.events);
			_.bindAll(this, 'mouseEnterEditText', 'mouseLeaveEditText', 'mouseEnterEditTo', 'acceptEdition', 'changeAmountRate', 'changeRatingTotalScore');
			this.template = _.template(editTempalte);

			this.model.bind('change:ratingTotalScore'
				, this.changeRatingTotalScore);
			this.model.bind('change:amountRate'
				, this.changeAmountRate);
		},

		render: function () {
			var html = this.template(this.model.toJSON());
			this.$el.html(html);
			
			// register tooltip
			this.$('.rate-btn').tooltip({
				html: true,
				trigger: 'manual',
				placement: 'right',
				animation: false,
				title: '<div class="rateit"></div>'
			});

			this.$('.rateit').rateit();

			return this;
		},

		changeAmountRate: function (comment, amountRate) {
			this.$('.report-rate-btn > .amount-rate')
			.text(amountRate);
		},

		changeRatingTotalScore: function (comment, totalScore) {
			var $reportRateBtn = $(this.$('.report-rate-btn')[0]),
				$rateitTotal = $reportRateBtn.siblings('.rateit');
			
			// update rate text
			$reportRateBtn.find('.total-score').text(totalScore);

			// update star
			$rateitTotal.data('rateit-value', totalScore);
			$rateitTotal.rateit();
		},

		mouseEnterEditText: function (e) {
			var $editText = $(e.currentTarget),
				text = $editText.text(),
        $postMessage = $editText.parents('.post').find('.editable').first(),
				sourceText = $postMessage.text(),
				beginAt = sourceText.indexOf(text),
				endAt = beginAt + text.length;

			$postMessage.html(sourceText.slice(0, beginAt));
			$postMessage.html($postMessage.html() + "<em class='highlight' data-toggle='tooltip' data-placement='top'>" 
				+ sourceText.slice(beginAt, endAt) + "</em>");
			$postMessage.html($postMessage.html() 
				+ sourceText.slice(endAt, sourceText.length));
            
      var tooltipTitle = $editText.siblings('.edit-to').text();
      $postMessage.find('em').tooltip({
          title: 'edit to: ' + tooltipTitle,
          placement: 'top',
          trigger: 'manual'
      }).tooltip('show');
		},
        
    mouseLeaveEditText: function (e) {
			var $editText = $(e.currentTarget),
	      $postMessage = $editText.parents('.post').find('.editable').first();
	    
	    $postMessage.find('em').tooltip('hide');
	    
	    $postMessage.find('.tooltip').remove();
	    
	    $postMessage.html($postMessage.text());
    },
    
    mouseEnterEditTo: function (e) {
      var $editTo = $(e.currentTarget);
      $editTo.tooltip('show');
        
    },
    
    acceptEdition: function (e) {
      console.log('acceptEdition')
    }
	});

	return EditView;
});