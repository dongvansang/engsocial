define(['views/components/search-item', 'collections/search-items']
, function (SearchItemView, SearchItems) {
	var SearchItemsView = Backbone.View.extend({
		el: '.scrollable-wrapper',

		initialize: function () {
			_.bindAll(this, 'render', 'reset', 'appendItem');
			this.collection = new SearchItems();
			this.collection.bind('add', this.appendItem);
			this.collection.bind('reset', this.reset);
			// render immediate when init
			this.render();
		},

		render: function () {
			this.$el.append('<ul id="search-results" class="list"></ul>');
		},

		appendItem: function (item) {
			var self = this;
			var searchItemView = new SearchItemView({
				model: item
			});
			var $searchItemHtml = searchItemView.render().$el;
			$searchItemHtml.hide();
			$('#search-results').append($searchItemHtml);
			$searchItemHtml.animate({
				height: 'toggle'
			}, {
				complete: function () {
					if (self.$('#search-results').height() > 600) {
						self.$el.slimScroll({
							height: 600
						});
					} else {
						self.$el.slimScroll({ destroy: '' });
						self.$el.attr('style', '');
					}
				}
			});
		},

		reset: function () {
			// destroy a model
			$(this.$('#search-results > .result-item')).animate({
				height: 'toggle'
			}, {
				duration: 500,
				complete: function () {
					$(this).remove();
				}
			});
		}
	});

	return SearchItemsView;
});