define(['models/comment', 'views/components/comments', 'views/components/post', 'text!templates/note.html']
, function (Comment, CommentsView, PostView, noteTemplate) {

	var NoteView = PostView.extend({

		template: _.template(noteTemplate),

		events: {
			'click .comment-btn': 'clickCommentBtn',
			'keypress .input-post': 'comment',
			'click .more-comment': 'loadMoreComment',
		},

		initialize: function () {
			this.events = _.extend({}, PostView.prototype.events, this.events);
			_.bindAll(this, 'render', 'clickCommentBtn', 'comment', 'onCommentSuccess', 'loadComments', 'loadMoreComment');
			this.commentsView = new CommentsView();

			this.commentsView.collection.bind('destroy'
				, this.onRemoveComment);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.$('.input-post').autosize();

			// append a comments view
			this.$('.list-comment-wrapper').html(this.commentsView.render().$el);
			return this;
		},

		clickCommentBtn: function (e) {
			var $commentTxt = this.$('.comment-txt');
			$commentTxt.toggle('fast');
			$commentTxt.focus();
		},

		loadComments: function (amount, page) {
			var self = this,
				$el = self.$el,
				postId = self.model.id;

			$.ajax({
				type: 'GET',
				url: '/api/comments',
				data: {
					postId: postId,
					amount: amount,
					page: page
				},
				statusCode: {
					200: function (comments) {
						_.sortBy(comments, function (comment) {
							return new Date(comment.createdIn);
						});

						_(comments).each(function (comment) {
							self.commentsView.collection.prepend(new Comment(comment));
						});
						
						var $listCommentWrapper = self.$('.list-comment-wrapper');
						if (self.model.get('amountComment') > self.commentsView.collection.length) {
							if ($listCommentWrapper.find('.more-comment').length === 0) {
								$listCommentWrapper.append('<div class="more more-comment"><span>more</span></div>');
							}
						} else {
							// if it is enough amount comment, remove more button
							$listCommentWrapper.find('.more-comment').detach();
						}

						// increase page
						self.model.increaseCommentPage(1);
					},
					404: function (message) {

					}
				}
			});
		},

		loadMoreComment: function () {
			this.loadComments(5, this.model.get('commentPage'));
		},

		comment: function (e) {
			var self = this,
				$commentTxt = $(e.currentTarget),
				postId = self.model.id,
				$reportCommentBtn = this.$('.report-comment-btn');

			// if press enter
			if (e.keyCode === 13) {
				// if don't hold shilt key on same time
				if (!e.shiftKey) {
					e.preventDefault();
					if ($commentTxt.val() === '') return;

					var content = $commentTxt.val();
					$commentTxt.val('');

					// active comment tab
					$reportCommentBtn.click();

					$commentTxt.attr('disabled', 'disabled');

					$.ajax({
						type: 'POST',
						url: '/api/comment',
						data: {
							content: content,
							// a comment have a different with a post that
							// it have parent
							parent: postId,
							isStatus: false
						},
						statusCode: {
							201: function (data) {
								self.onCommentSuccess(new Comment(data));
							}
						}
					});
				}
			}
		}, // end comment function
		onCommentSuccess: function (comment) {
			var $reportCommentBtn = this.$('.report-comment-btn');

			this.$('.comment-txt').removeAttr('disabled')	;

			function changeReportComment (plusAmount) {
				var string = $reportCommentBtn.text().trim()
					, amount = string.slice(0, string.indexOf(' '))
				return (parseInt(amount, 10) + plusAmount) + ' comment';
			}
			// increase comment amount
			$reportCommentBtn.text(changeReportComment(1));
			this.commentsView.collection.append(comment);
		},

		onRemoveComment: function () {
			var $reportCommentBtn = this.$('.report-comment-btn');

			function changeReportComment (plusAmount) {
				var string = $reportCommentBtn.text().trim()
					, amount = string.slice(0, string.indexOf(' '))
				return (parseInt(amount, 10) + plusAmount) + ' comment';
			}
			// increase comment amount
			$reportCommentBtn.text(changeReportComment(-1));
		},

	});
	
	return NoteView;
});