define(['text!templates/comment.html', 'views/components/post', 'Backbone']
, function (commentTempalte, PostView) {

	var CommentView;
	CommentView = PostView.extend({

		initialize: function () {
			this.events = _.extend({}, PostView.prototype.events, this.events);
			_.bindAll(this, 'render', 'changeAmountRate', 'changeRatingTotalScore');
			this.template = _.template(commentTempalte);

			this.model.bind('change:ratingTotalScore'
				, this.changeRatingTotalScore);
			this.model.bind('change:amountRate'
				, this.changeAmountRate);
		},

		changeAmountRate: function (comment, amountRate) {
			this.$('.report-rate-btn > .amount-rate')
			.text(amountRate);
		},

		changeRatingTotalScore: function (comment, totalScore) {
			var $reportRateBtn = $(this.$('.report-rate-btn')[0]),
				$rateitTotal = $reportRateBtn.siblings('.rateit');
			
			// update rate text
			$reportRateBtn.find('.total-score').text(totalScore);

			// update star
			$rateitTotal.data('rateit-value', totalScore);
			$rateitTotal.rateit();
		},

		render: function () {
			var html = this.template(this.model.toJSON());
			this.$el.html(html);

			// load rate plus in for this comment
			this.$('.rateit').rateit();

			// register tooltip
			this.$('.rate-btn').tooltip({
				html: true,
				trigger: 'manual',
				placement: 'right',
				animation: false,
				title: '<div class="rateit"></div>'
			});

			return this;
		}
	});
	return CommentView;
});