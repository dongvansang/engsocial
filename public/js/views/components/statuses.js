define(['collections/statuses', 'views/components/status', 'models/comment', 'Backbone'], function (Statuses, StatusView, Comment) {

	var StatusesView;
	StatusesView = Backbone.View.extend({
		tagName: 'ul',
		id: 'list-post',

		initialize: function () {
			// every function that use 'this' as the current object
			// should be in here
			_.bindAll(this, 'render', 'addStatus');
			this.collection = new Statuses();
			this.collection.bind('add', this.addStatus);
			this.render();
		},

		render: function () {
			this.$el.html('');
			return this;
		},

		addStatus: function (status) {
			var statusView = new StatusView({
				model: status
			});

			// delegate to each status view render by its self
			var $html = statusView.render().$el;
			$html.hide();
			this.$el.prepend($html);
			$html.fadeIn('slow');

			statusView.loadComments(5, 1);
			statusView.loadEdits(5, 1);
			statusView.$('.report-comment-btn').click();
		}
	});

	return StatusesView;
});