define(['text!templates/chat-user.html', 'Backbone']
, function (chatUserTemplate) {
	var ChatUserView;
	ChatUserView = Backbone.View.extend({
		tagName: 'li',

		template: _.template(chatUserTemplate),

		events: {
			'click .chat-user': 'openChatDock'
		},

		initialize: function () {
			_.bindAll(this, 'render', 'openChatDock', 'onOnline');
			this.model.bind('change:isOnline', this.onOnline);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.$el.attr('id', this.model.id);
			return this;
		},

		onOnline: function (chatUser, isOnl) {
			var $i = this.$('.contact-status-wrapper > i');
			if (isOnl) {
				$i.addClass('online');
				$i.removeClass('offline');
			} else {
				$i.addClass('offline');
				$i.removeClass('online');	
			}
		},

		openChatDock: function () {
			this.options.model.collection.trigger('openDock', this.model);
		}
	});

	return ChatUserView;
});