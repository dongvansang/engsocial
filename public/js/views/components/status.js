define(['views/components/post', 'views/components/comment', 'views/components/comments', 'views/components/edit', 'views/components/edits', 'text!templates/status.html', 'models/comment', 'models/edit', 'text!templates/popup-edit.html', 'jQuery.slimScroll', 'Backbone'],
	function (PostView, CommentView, CommentsView, EditView, EditsView, statusTemplate, Comment, Edit, popoverEditTemplate) {

	var StatusView = PostView.extend({

		// The DOM event specific to an item
		events: {
			'click .comment-btn': 'clickCommentBtn',
			'click .note-btn': 'clickNoteBtn',
			'keypress .input-post': 'comment',
			'mouseup .status': 'selectText',
			'submit #editFrm': 'edit',
			'click .more-comment': 'loadMoreComment',
			'click .more-edit': 'loadMoreEdit'
		},

		initialize: function () {
			this.events = _.extend({}, PostView.prototype.events, this.events);
			_.bindAll(this, 'render', 'clickCommentBtn', 'loadComments', 'comment', 'loadEdits', 'edit', 'onRemoveComment', 'changeRatingTotalScore', 'changeAmountRate', 'onCommentSuccess', 'loadMoreComment', 'loadMoreEdit', 'selectText');

			// create a view of comment collection
			this.commentsView = new CommentsView();

			// create a view of edit collection
			this.editsView = new EditsView();

			this.commentsView.collection.bind('destroy'
				, this.onRemoveComment);

			this.model.bind('change:ratingTotalScore'
				, this.changeRatingTotalScore);
			this.model.bind('change:amountRate'
				, this.changeAmountRate);

			this.template = _.template(statusTemplate);
		},

		render: function () {
			var html = this.template(this.model.toJSON());
			this.$el.html(html);

			// register tooltip
			this.$('.rate-btn').tooltip({
				html: true,
				trigger: 'manual',
				placement: 'right',
				animation: false,
				title: '<div class="rateit"></div>'
			});

			this.$('.rateit').rateit();

			// reregister autosize plusin for all input-post
			// this make sure that new comment textare will receive it
			this.$('.input-post').autosize();

			// append a comments view
			this.$('.list-comment-wrapper').html(this.commentsView.render().$el);

			// append a edits view
			this.$('.list-edit-wrapper').html(this.editsView.render().$el);

			// for chainable call, like .render().el
			return this;
		},

		changeAmountRate: function (status, amountRate) {
			$(this.$('.report-rate-btn > .amount-rate')[0])
			.text(amountRate);
		},

		changeRatingTotalScore: function (status, totalScore) {
			var $reportRateBtn = this.$('.report-rate-btn').first(),
				$rateitTotal = $reportRateBtn.siblings('.rateit');
			
			// update rate text
			$reportRateBtn.find('.total-score').text(totalScore);

			// update star
			$rateitTotal.data('rateit-value', totalScore);
			$rateitTotal.rateit();
		},

		onRemoveComment: function () {
			var $reportCommentBtn = this.$('.report-comment-btn');

			function changeReportComment (plusAmount) {
				var string = $reportCommentBtn.text().trim()
					, amount = string.slice(0, string.indexOf(' '))
				return (parseInt(amount, 10) + plusAmount) + ' comment';
			}
			// increase comment amount
			$reportCommentBtn.text(changeReportComment(-1));
		},

		loadEdits: function (amount, page) {
			var self = this,
				$el = self.$el,
				postId = self.model.id;

			$.ajax({
				type: 'GET',
				url: '/api/edits',
				data: {
					postId: postId,
					amount: amount,
					page: page
				},
				statusCode: {
					200: function (edits) {
						_.sortBy(edits, function (edit) {
							return new Date(edit.createdIn);
						});

						_(edits).each(function (edit) {
							self.editsView.collection.add(new Edit(edit));
						});
						
						var $listEditWrapper = self.$('.list-edit-wrapper');
						if (self.model.get('amountEdit') > self.editsView.collection.length) {
							if ($listEditWrapper.find('.more-edit').length === 0) {
								$listEditWrapper.append('<div class="more more-edit"><span>more</span></div>');
							}
						} else {
							// if it is enough amount edit, remove more button
							$listEditWrapper.find('.more-edit').detach();
						}

						// increase page
						self.model.increaseEditPage(1);
					}
				}
			});
		},

		loadComments: function (amount, page) {
			var self = this,
				$el = self.$el,
				postId = self.model.id;

			$.ajax({
				type: 'GET',
				url: '/api/comments',
				data: {
					postId: postId,
					amount: amount,
					page: page
				},
				statusCode: {
					200: function (comments) {
						_.sortBy(comments, function (comment) {
							return new Date(comment.createdIn);
						});

						_(comments).each(function (comment) {
							self.commentsView.collection.prepend(new Comment(comment));
						});
						
						var $listCommentWrapper = self.$('.list-comment-wrapper');
						if (self.model.get('amountComment') > self.commentsView.collection.length) {
							if ($listCommentWrapper.find('.more-comment').length === 0) {
								$listCommentWrapper.append('<div class="more more-comment"><span>more</span></div>');
							}
						} else {
							// if it is enough amount comment, remove more button
							$listCommentWrapper.find('.more-comment').detach();
						}

						// increase page
						self.model.increaseCommentPage(1);
					},
					404: function (message) {

					}
				}
			});
		},

		clickCommentBtn: function () {
			var $commentTxt = this.$('.comment-txt');
			$commentTxt.toggle('fast');
			$commentTxt.focus();
		},

		clickNoteBtn: function (e) {
			e.stopPropagation();
			e.preventDefault();

			var postId = this.model.id,
				nickname = this.model.get('nickname'),
				$noteModal = $('#note-modal');
			
			$noteModal.find('#refAt').attr('value', postId);
			$noteModal.find('#refOwner').attr('value', nickname);
			$noteModal.modal('show');
		},

		comment: function (e) {
			var self = this,
				$commentTxt = $(e.currentTarget),
				postId = self.model.id,
				$reportCommentBtn = this.$('.report-comment-btn');

			// if press enter
			if (e.keyCode === 13) {
				// if don't hold shilt key on same time
				if (!e.shiftKey) {
					e.preventDefault();
					e.stopPropagation();
					if ($commentTxt.val() === '') return;

					var content = $commentTxt.val();
					$commentTxt.val('');

					// active comment tab
					$reportCommentBtn.click();

					$commentTxt.attr('disabled', 'disabled');

					$.ajax({
						type: 'POST',
						url: '/api/comment',
						data: {
							content: content,
							// a comment have a different with a post that
							// it have parent
							parent: postId,
							isStatus: true
						},
						statusCode: {
							201: function (data) {
								self.onCommentSuccess(new Comment(data));
							}
						}
					});
				}
			}
		}, // end comment function

		onCommentSuccess: function (comment) {
			var $reportCommentBtn = this.$('.report-comment-btn');

			this.$('.comment-txt').removeAttr('disabled')	;

			function changeReportComment (plusAmount) {
				var string = $reportCommentBtn.text().trim()
					, amount = string.slice(0, string.indexOf(' '))
				return (parseInt(amount, 10) + plusAmount) + ' comment';
			}
			// increase comment amount
			$reportCommentBtn.text(changeReportComment(1));
			this.commentsView.collection.append(comment);
		},

		edit: function (e) {
			e.stopPropagation();
			e.preventDefault();
			var $editFrm = $(e.currentTarget),
				postId = this.model.id,
				self = this;

				// active edit tab
				var $reportEditBtn = self.$('.report-edit-btn');
				$reportEditBtn.click();

			$.ajax({
				type: $editFrm.attr('method'),
				url: $editFrm.attr('action'),
				data: $editFrm.serialize() + "&postId=" + postId,
				statusCode: {
					201: function (edit) {
						$('em').popover('hide');

						// recover content
						$('.post-message')
							.first()
						.html(self.model.get('content'));

						function changeReportEdit (plusAmount) {
							var string = $reportEditBtn.text().trim()
								, amount = string.slice(0, string.indexOf(' '))
							return (parseInt(amount, 10) + plusAmount) + ' edits';
						}
						// increase edit amount
						$reportEditBtn.text(changeReportEdit(1));

						self.editsView.collection.add(new Edit(edit));
					},
					500: function (err) {
						console.log(err)
					}
				}
			});

			// prevent form default submit event
			return false;
		},

		loadMoreComment: function () {
			this.loadComments(5, this.model.get('commentPage'));
		},

		loadMoreEdit: function () {
			this.loadEdits(5, this.model.get('editPage'));
		},

		selectText: function () {
			var range = getSelectionRange();

			if (range === null) return;

			// if selection is text
			var selectedText = range.toString().trim();
			if (selectedText) {
				var selParentE1 = range.commonAncestorContainer;
				if (selParentE1.nodeType == 3) {
					selParentE1 = selParentE1.parentNode;
					var $selParentE1 = $(selParentE1);
					if ($selParentE1.hasClass('editable')) {
						// element text
						var eText = $selParentE1.text()

						// reformat p tag
						$selParentE1.html(eText);
						
						// begin of selection
						var beginAt = range.startOffset,
							// end of selection
							endAt = range.endOffset;

						$selParentE1.html(eText.slice(0, beginAt));
						$selParentE1.html($selParentE1.html() + "<em data-toggle='popover'>" + eText.slice(beginAt, endAt) + "</em>");
						$selParentE1.html($selParentE1.html() + eText.slice(endAt, eText.length));

						var templateCompiler = _.template(popoverEditTemplate)

						$selParentE1.find('em').popover({
							html: 'true',
							placement: 'bottom',
							trigger: 'click',
							content: templateCompiler({ editText: selectedText })
						});

						$selParentE1.find('#edit-txt').autosize();
					}
				}
			}
		}
	});

	function getSelectionRange() {
		var sel;
		if (window.getSelection) {
			sel = window.getSelection();
			if (sel.rangeCount > 0) {
				return sel.getRangeAt(0);
			}
		} else if (document.selection) {
			return document.selection.createRange();
		}
		return null;
	}

	return StatusView;
});