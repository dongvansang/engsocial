define(['Backbone'], function () {
	var tooltipTimeout, leavePostTimeout;
	function delay (timeout, time, func) {
		timeout = setTimeout(func, time);
	}

	var PostView = Backbone.View.extend({
		tagName: 'li',

		events: {
			'click .like-btn': 'like',
			'mouseenter .post-main-content': 'mouseEnterPost',
			'mouseleave .post-main-content': 'mouseLeavePost',
			'mouseenter .rate-btn': 'mouseEnterRateBtn',
			'mouseleave .rate-btn': 'mouseLeaveRateBtn',
			'click .close': 'deletePost'
		},

		initialize: function () {
			_.bindAll(this, 'like', 'mouseEnterPost', 'mouseLeavePost', 'deletePost', 'comment', 'mouseLeaveRateBtn', 'mouseEnterRateBtn');
		},
		
		mouseEnterPost: function (e) {
			e.preventDefault();
			e.stopPropagation();
			// invisible another close btn
			this.$('.close:not(e.currentTarget)').hide();

			this.$('.close').first()
			.attr('style', 'display: inline-block;');
		},

		mouseLeavePost: function (e) {
			this.$('.close').first()
			.attr('style', 'display: none;');
		},

		mouseEnterRateBtn: function (e) {
			e.preventDefault();
			e.stopPropagation();

			var $rateBtn = $(e.currentTarget),
				postId = this.model.id,
				self = this;

			function rate (value) {
				// update rate control
				$rateBtn.data('score', value);

				$.ajax({
					type: 'POST',
					url: '/rating',
					data: {
						postId: postId,
						score: value
					},
					statusCode: {
						200: function (totalScore) {
							var isNewRating = true,
								rates = self.model.get('rates');

							// model update
							self.model.set('rated', value);

							// if rate score equal 0
							// remove this user from list user rated
							_(rates).each(function (rate, index) {
								if (rate.userId === loggedInUser) {
									if (value === 0) {
										rates.splice(index, 1);
									} else {
										rate.score = value;
									}
									isNewRating = false;
								}
							});

							// if this is new rating,
							// push into rates
							if (isNewRating === true && value != 0) {
								rates.push({ userId: loggedInUser, score: value });
							}

							// fire event rates:change
							self.model.trigger('change:rates', rates);
						}
					}
				});
			}

			clearTimeout(tooltipTimeout);
			$('[data-toggle="tooltip"]').tooltip('hide');
			$rateBtn.tooltip('show')
				.siblings('.tooltip')
			.find('.rateit')
			.rateit({
				max: 4,
				value: $rateBtn.data('score')
			})
			.bind('rated', function (e, value) {
				rate(value);
			})
			.bind('reset', function () {
				rate(0);
			});
		}, // end mouseenter rate-btn

		mouseLeaveRateBtn: function (e) {
			e.preventDefault();
			e.stopPropagation();
			
      var $rateBtn = $(e.currentTarget);

			tooltipTimeout = setTimeout(function () {
				$rateBtn.tooltip('hide');
			}, 500);
			$('.tooltip').mouseenter(function () {
				clearTimeout(tooltipTimeout); 
			}).mouseleave(function () {
				tooltipTimeout = setTimeout(function () {
					$rateBtn.tooltip('hide');
				}, 500);
			});
		},

		deletePost: function (e) {
			// prevent a call delete to status's comment
			e.preventDefault();
			e.stopPropagation();
			var $el = this.$el;
			var self = this;
			this.model.destroy({
				success: function (model, res) {
					$el.fadeOut('slow', function () {
						$el.remove();
					});
				},
				error: function () {
					// TODO hanlder error
				}
			});
		},

		like: function (e) {
			e.preventDefault();
			e.stopPropagation();
			var $likeBtn = $(e.currentTarget)
				, postId = this.model.id
				, $reportLikeBtn = this.$('.report-like-btn').first();
			
			function changeReportLike (plusAmount) {
				var string = $reportLikeBtn.text().trim()
					, amount = string.slice(0, string.indexOf(' '))
				return (parseInt(amount, 10) + plusAmount) + ' like';
			}

			if ($likeBtn.text().trim() === 'Like') {
				$likeBtn.text('Unlike');
				$.ajax({
					type: 'POST',
					url: '/like',
					data: {
						postId: postId
					},
					statusCode: {
						200: function (data) {
							// increase like 1 unit
							$reportLikeBtn.text(changeReportLike(1));
						}
					}
				});
			} else {
				$likeBtn.text('Like');
				$.ajax({
					type: 'POST',
					url: '/unlike',
					data: {
						postId: postId
					},
					statusCode: {
						200: function (data) {
							// decrease like 1 unit
							$reportLikeBtn.text(changeReportLike(-1));
						}
					}
				});
			}
		} // end like function
	});
	
	return PostView;
});