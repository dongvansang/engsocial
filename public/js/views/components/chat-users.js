define(['collections/chat-users', 'views/components/chat-user', 'models/chat-user', 'Backbone']
, function (ChatUsers, ChatUserView, ChatUser) {

	var ChatUsersView;
	ChatUsersView = Backbone.View.extend({
		tagName: 'ul',
		id: 'list-chat-users',

		initialize: function () {
			_.bindAll(this, 'syncCollection', 'resetCollection');
			this.collection = new ChatUsers();
			this.collection.bind('sync', this.syncCollection);
			this.collection.bind('reset', this.resetCollection);
		},

		render: function () {
			this.$el.html('');
			return this;
		},

		syncCollection: function (chatUsers, resp, option) {
			var self = this;

			// store origin version, this is a backup when perform a searching
			self.originCollection = chatUsers;

			_(chatUsers.models).each(function (chatUser) {
				var chatUserView = new ChatUserView({
					model: chatUser
				});
				self.$el.append(chatUserView.render().$el);
			});
			
		},

		resetCollection: function (chatUsers, option) {
			var self = this;

			if (chatUsers.length === 0) {
				self.$el.html('<div class="not-found">not found</div>');
			} else {
				self.$el.html('');
				_(chatUsers.models).each(function (chatUser) {
					var chatUserView = new ChatUserView({
						model: chatUser
					});
					self.$el.append(chatUserView.render().$el);
				});	
			}	
		}
	});

	return ChatUsersView;
});