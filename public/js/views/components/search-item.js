define(['text!templates/search-item.html', 'Backbone']
, function (searchItemTemplate) {
	var SearchItemView = Backbone.View.extend({
		tagName: 'li',

		className: 'result-item',

		template: _.template(searchItemTemplate),

		events: {
			'click .add-friend-btn': 'addFriend'
		},
		
		initialize: function () {
			_.bindAll(this, 'render', 'addFriend');
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			this.$('.rateit').rateit();
			// accept chaining call
			return this;
		},

		addFriend: function (e) {
			var userId = this.model.id,
				$addFriendBtn = $(e.currentTarget);
				
			if ($addFriendBtn.text().trim() === 'Unfriend') {
				$addFriendBtn.text('Add friend');
				$.ajax({
					type: 'DELETE',
					url: '/api/friend/' + userId
				});
			} else {
				$addFriendBtn.text('Unfriend');
				$.ajax({
					type: 'POST',
					url: '/api/friend/' + userId
				});
			}
		}
	});
	
	return SearchItemView;
});