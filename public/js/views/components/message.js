define(['models/message', 'text!templates/message.html', 'Backbone']
, function (Message, messageTemplate) {
	var MessageView;

	MessageView = Backbone.View.extend({
		tagName: 'li',
		className: 'message-item',

		template: _.template(messageTemplate),

		events: {
			'mouseenter .message-content': 'onMouseEnter',
			'mouseleave .message-content': 'onMouseLeave'
		},

		initialize: function () {
			_.bindAll(this, 'render');
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			// tooltip on hover
			this.$('[data-toggle="tooltip"]').tooltip();
			return this;
		},

		onMouseEnter: function () {
			this.$('.short-timestamp').attr('style', 'display: inline;');
		},

		onMouseLeave: function () {
			this.$('.short-timestamp').attr('style', 'display: none;');
		}
	});

	return MessageView;
});