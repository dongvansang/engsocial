define(['collections/edits', 'views/components/edit', 'Backbone']
, function (Edits, EditView) {

	var EditsView;
	EditsView = Backbone.View.extend({
		tagName: 'ul',
		id: 'list-edit',

		initialize: function () {
			_.bindAll(this, 'render', 'addEdit');
			this.collection = new Edits();
			this.collection.bind('add', this.addEdit);
		},

		render: function () {
			this.$el.html('');
			return this;
		},

		addEdit: function (edit) {
			var editView = new EditView({
				model: edit
			});
			
			// append new edit to #list-edit
			var $html = editView.render().$el;
			$html.hide();
			this.$el.append($html);
			$html.fadeIn('slow');
		}
	});

	return EditsView;
});