define(['text!templates/chat-dock.html', 'views/components/conversation', 'models/message', 'collections/conversation', 'Backbone']
, function (chatDockTemplate, ConversationView, Message, Conversation) {
	var ChatDockView;
	ChatDockView = Backbone.View.extend({
		tagName: 'li',
		className: 'chat-dock pull-right',

		template: _.template(chatDockTemplate),
		events: {
			'click .close': 'close',
			'keypress .input-msg-txt': 'sendMsg'
		},

		initialize: function () {
			_.bindAll(this, 'render', 'close', 'sendMsg', 'receiveMsg', 'loadMsg', 'onOnline');
			this.model.set('messages', new Conversation());
			this.conversationView = new ConversationView({
				collection: this.model.get('messages')
			});

			this.model.bind('receiveMsg', this.receiveMsg);
			this.model.bind('change:isOnline', this.onOnline);
		},

		render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			
			this.$el.attr('id', this.model.get('receiver').id);
			
			this.$('.scrollable')
				.append(this.conversationView.render().$el);
			
			this.$('.input-msg-txt').autosize();
			
			this.$('.input-msg-txt').focus();

			// register scroll
			this.$('.scrollable').slimScroll({
				height: 240
			});

			this.loadMsg();

			return this;
		},

		onOnline: function (chatUser, isOnl) {
			var $i = this.$('.chat-dock-status > i');
			if (isOnl) {
				$i.addClass('online');
				$i.removeClass('offline');
			} else {
				$i.addClass('offline');
				$i.removeClass('online');	
			}
		},

		loadMsg: function () {
			var self = this;
			$.ajax({
				type: 'GET',
				url: '/api/messages',
				data: {
					chatWithUser: self.model.get('receiver').id
				},
				statusCode: {
					200: function (messages) {
						_(messages).each(function (msg) {
							var formartedMsg = {};
							if (msg.senderId === self.model.get('sender').id) {
								formartedMsg.user = {
									nickname: self.model.get('sender').nickname,
									avatar: self.model.get('sender').avatar
								}
							} else {
								formartedMsg.user = {
									nickname: self.model.get('receiver').nickname,
									avatar: self.model.get('receiver').avatar
								}
							}
							formartedMsg.msg = msg.message;
							formartedMsg.timestamp = msg.timestamp;

							self.model.addMsg(new Message(formartedMsg));	
						});
					}
				}
			});
		},

		receiveMsg: function (msgObj) {
			this.model.addReceiveMsg(new Message({
				user: {
					nickname: this.model.get('receiver').nickname,
					avatar: this.model.get('receiver').avatar
				},
				msg: msgObj.msgData.msg,
				timestamp: msgObj.msgData.timestamp
			}));
		},

		sendMsg: function (e) {
			$inputMsgTxt = $(e.currentTarget);

			if (e.keyCode === 13) {
				if (!e.shiftKey) {
					e.preventDefault();
					e.stopPropagation();
					if ($inputMsgTxt.val() === '') return;

					var msg = $inputMsgTxt.val();
					$inputMsgTxt.val('');

					var nickname = readCookie('nickname'),
						timestamp = new Date(),
						avatar = readCookie('avatar');

					this.model.addSendMsg(new Message({
						user: {
							nickname: this.model.get('sender').nickname,
							avatar: this.model.get('sender').avatar
						},
						msg: msg,
						timestamp: timestamp
					}));
				}
			}
		},

		close: function (e) {
			this.$el.detach();
		}

	});

	return ChatDockView;
});