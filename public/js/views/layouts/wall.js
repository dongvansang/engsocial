// require Backbone
define(['views/components/statuses', 'models/status', 'Backbone']
, function(StatusesView, Status) {
		
	var WallView = Backbone.View.extend({
		el: $('.main-content-wrapper'),

		statusesView: new StatusesView(),

		initialize: function () {
			_.bindAll(this, 'render');
			this.render();
		},

		events: {
			'submit #note-frm': 'note',
			'keypress .status-txt': 'addStatus'
		},
		
		render: function () {
			var self = this;

			self.$el.append(self.statusesView.render().$el);

			self.$('.input-post').autosize();

			// request top 10 statuses
			$.ajax({
				type: 'GET',
				url: '/api/statuses',
				data: {
					amount: 10,
					page: 1,
					userId: 'me'
				},
				statusCode: {
					200: function (statuses) {
						_(statuses).each(function (status) {
							self.statusesView.collection.add(new Status(status));
						});
					}
				}
			});
		},

		note: function (e) {
			e.preventDefault();
			e.stopPropagation();

			var $noteFrm = $(e.currentTarget)
				, postId = $('#note-modal').attr('data-postId')
				, frmObj = {}
				, tags = [];

			// get form data
			$.each($noteFrm.serializeArray(), function(_, kv) {
				frmObj[kv.name] = kv.value;
			});

			tags = frmObj['tags'].trim().split(/\*s*,\*s*/);
			console.log('TODO insert tags: ',  tags)
			$.ajax({
				type: 'POST',
				url: '/api/note',
				// get data from note form
				data: {
					title: frmObj['title'],
					tags: tags,
					refAt: postId
				},
				statusCode: {
					200: function (data) {
						displayMessage({
							title: 'Added note',
							message: 'Please check on your Note page'
						}, 'success');
					},
					500: function (err) {
						// convert responseText to json
						var errJSON = $.parseJSON(err.responseText);
						displayMessage(errJSON, 'warning');
					}
				}
			});

			// close modal
			$('#note-modal').modal('hide');
		},

		addStatus: function (e) {
			var self = this,
				$statusTxt = $(e.currentTarget);

			// if press enter
			if (e.keyCode === 13) {
				// if don't hold shilt key on same time
				if (!e.shiftKey) {
					e.preventDefault();
					if ($statusTxt.val() === '') return;

					var content = $statusTxt.val();
					$statusTxt.val('');
					$.ajax({
						type: 'POST',
						url: '/api/status',
						data: {
							content: content
						},
						statusCode: {
							201: function (status) {
								self.statusesView.collection.add(status);
							}
						}
					});
				}
			}
		}
	});

	return WallView;
});