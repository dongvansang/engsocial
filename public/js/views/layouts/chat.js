define(['views/components/chat-users', 'collections/chat-users', 'views/components/chat-docks', 'models/chat-dock', 'models/message', 'Backbone']
, function (ChatUsersView, ChatUsers, ChatDocksView, ChatDock, Message) {

	var ChatView = Backbone.View.extend({
		el: '.chat-sidebar-wrapper',

		events: {
			'keyup #search-chat-user': 'searchChatUser',
			'click .chat-header': 'active'
		},

		initialize: function () {
			_.bindAll(this, 'render', 'openChatDock', 'sendMsg', 'receiveMsg', 'userOnline', 'userOffline', 'active');

			// a collection chat user
			this.chatUsers =  new ChatUsers();
			this.chatUsers.bind('openDock', this.openChatDock);
			this.chatUsersView = new ChatUsersView();
			this.chatDocksView = new ChatDocksView();
			
			this.render();

			this.socket = io.connect();
			this.socket.on('receiveMsg', this.receiveMsg);
			this.socket.on('userOnline', this.userOnline);
			this.socket.on('userOffline', this.userOffline);
		},

		render: function () {
			var self = this;

			this.$('.chat-sidebar').prepend(this.chatUsersView.render().$el);

			// height of .chat-sidebar
			var height = window.innerHeight - 60 - 32 - 30 - 5;

			// layout chat sidebar
			if (window.innerWidth < 979) {
				this.$el.addClass('dock');
				var chatSidebarWrapperHeight = window.innerHeight - 32;
				this.$el.attr('style', 'top: ' + chatSidebarWrapperHeight + 'px');
			} else {
				this.$el.removeClass('dock');
				height += 20;
			}

			height = height < 100 ? 100 : height;
			this.$el.find('.chat-sidebar').height(height);

			this.chatUsers.fetch({
				data: { userId: 'me' },
				success: function (chatUsers) {
					self.chatUsersView.collection.reset(chatUsers.models);
				}
			});
		},

		sendMsg: function (msgObj) {
			this.socket.emit('message', msgObj);
		},

		userOnline: function (userId) {
			_(this.chatUsers.models).each(function (chatUser) {
				if(chatUser.id === userId)
					chatUser.set('isOnline', true);
			});

			_(this.chatDocksView.collection.models)
			.each(function (chatDock) {
				if(chatDock.id === userId)
					chatDock.set('isOnline', true);
			});
		},

		userOffline: function (userId) {
			_(this.chatUsers.models).each(function (chatUser) {
				if(chatUser.id === userId)
					chatUser.set('isOnline', false);
			});

			_(this.chatDocksView.collection.models)
			.each(function (chatDock) {
				if(chatDock.id === userId)
					chatDock.set('isOnline', false);
			});
		},

		receiveMsg: function (msgObj) {
			// chatDock's status is none
			var isOpened = false,
				self = this;
			_(self.chatDocksView.collection.models)
			.each(function (chatDock) {
				if (chatDock.get('receiver').id === msgObj.senderId) {
					if (self.chatDocksView.$('#' + msgObj.senderId).length === 1) {
						chatDock.addMsg(new Message({
							user: {
								nickname: chatDock.get('receiver').nickname,
								avatar: chatDock.get('receiver').avatar
							},
							msg: msgObj.msgData.msg,
							timestamp: msgObj.msgData.timestamp
						}));
					} else {
						_(self.chatUsers.models).each(function (chatUser) {
							if (chatUser.id === msgObj.senderId) {
								self.openChatDock(chatUser);
							}
						});
					}
					isOpened = true;
				}
			});

			// if not yet received, popup a dock
			if (!isOpened) {
				var self = this;
				_(this.chatUsers.models).each(function (chatUser) {
					if (chatUser.id === msgObj.senderId) {
						self.openChatDock(chatUser);
					}
				})
			}
		},

		openChatDock: function (chatUser) {
			var openFlag = false,
				self = this;
				
			_(this.chatDocksView.collection.models).each(function (chatDock) {
				if (chatUser.get('id') === chatDock.get('receiver').id) {
					if (self.chatDocksView.$('#' + chatUser.get('id')).length === 0) {
						self.chatDocksView.addChatDock(chatDock);
					}
					openFlag = true;
				}
			});

			// if exist this dock
			if (openFlag) {
				this.chatDocksView.highlight(chatUser.id);
				return;
			}

			var chatDock = new ChatDock({
				id: chatUser.id,
				sender: {
					id: readCookie('loggedInUser'),
					nickname: readCookie('nickname'),
					avatar: decodeURIComponent(readCookie('avatar'))
				},
				receiver: {
					id: chatUser.get('id'),
					nickname: chatUser.get('nickname'),
					avatar: chatUser.get('avatar')
				},
				isOnline: chatUser.get('isOnline')
			});
			chatDock.bind('sendMsg', this.sendMsg);
			this.chatDocksView.collection.add(chatDock);
		},

		searchChatUser: function (e) {
			var $searchTxt = $(e.currentTarget),
				keyword = $searchTxt.val().trim();
				
			if (keyword === '') {
				// reset to origin list user
				this.chatUsersView.collection.reset(this.chatUsers.models);
			} else {
				var matchUsers = this.chatUsers.byNickname(keyword);
				this.chatUsersView.collection.reset(matchUsers.models);	
			}
		},

		active: function (e) {
			if (this.$el.hasClass('dock') === false) return;

			if (this.$el.hasClass('active')) {
				this.$el.removeClass('active');
				var height = window.innerHeight - 32;
				this.$el.attr('style', 'top: ' + height + 'px');
			} else {
				this.$el.addClass('active');
				this.$el.removeAttr('style');
			}
		}
	});

	return ChatView;
});