// require Backbone
define(['views/components/statuses', 'models/status', 'Backbone']
, function(StatusesView, Status) {
		
	var SiblingWallView = Backbone.View.extend({
		el: $('.main-content-wrapper'),

		statusesView: new StatusesView(),

		initialize: function () {
			_.bindAll(this, 'render');
			this.render();
		},

		events: {
			'submit #note-frm': 'note'
		},
	
		render: function () {
			var self = this;

			self.$el.append(self.statusesView.render().$el);

			// request top 10 statuses
			$.ajax({
				type: 'GET',
				url: '/api/statuses',
				data: {
					amount: 10,
					page: 1,
					userId: $('.sibling-wall').attr('id')
				},
				statusCode: {
					200: function (statuses) {
						_(statuses).each(function (status) {
							self.statusesView.collection.add(new Status(status));
						});
					}
				}
			});
		},

		note: function (e) {
			e.preventDefault();
			e.stopPropagation();

			var $noteFrm = $(e.currentTarget)
				, postId = $('#note-modal').attr('data-postId')
				, frmObj = {}
				, tags = [];

			// get form data
			$.each($noteFrm.serializeArray(), function(_, kv) {
				frmObj[kv.name] = kv.value;
			});

			tags = frmObj['tags'].trim().split(/\*s*,\*s*/);
			console.log('TODO insert tags: ',  tags)
			$.ajax({
				type: 'POST',
				url: '/api/note',
				// get data from note form
				data: {
					title: frmObj['title'],
					tags: tags,
					refAt: postId
				},
				statusCode: {
					200: function (data) {
						displayMessage({
							title: 'Added note',
							message: 'Please check on your Note page'
						}, 'success');
					},
					500: function (err) {
						// convert responseText to json
						var errJSON = $.parseJSON(err.responseText);
						displayMessage(errJSON, 'warning');
					}
				}
			});

			// close modal
			$('#note-modal').modal('hide');
		}
	});

	return SiblingWallView;
});