define(['views/components/notes','models/note', 'jQuery.slimScroll', 'Backbone']
, function (NotesView, Note) {

	var PersonalNoteView;
	PersonalNoteView = Backbone.View.extend({
		el: $('.main-content-wrapper'),

		tagName: 'ul',

		id: 'list-post',

		className: 'main-list',

		notesView: new NotesView(),

		initialize: function () {
			_.bindAll(this, 'render');
			this.render();
		},

		render: function () {
			this.$el.append(this.notesView.render().$el);
			this.notesView.loadNotes(10, 1);
		}
	});

	return PersonalNoteView;
});