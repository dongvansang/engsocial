// require Backbone
define(['views/components/statuses', 'models/status', 'Backbone']
, function(StatusesView, Status) {
		
	var NewsFeedView = Backbone.View.extend({
		el: $('.main-content-wrapper'),

		statusesView: new StatusesView(),

		initialize: function () {
			_.bindAll(this, 'render');
			this.render();
		},
	
		render: function () {
			var self = this;
			self.$el.append(self.statusesView.$el);

			// request top 10 statuses
			$.ajax({
				type: 'GET',
				url: '/api/newsfeeds',
				statusCode: {
					200: function (statuses) {
						_(statuses).each(function (status) {
							self.statusesView.collection.add(new Status(status));
						});
					}
				}
			});
		}
	});

	return NewsFeedView;
});