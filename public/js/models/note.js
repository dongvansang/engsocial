define(['models/post'], function (Post) {
	var monthName = [
		"Jan", "Feb", "Mar",
		"Apr", "May", "Jun",
		"Jul", "Aug", "Sep",
		"Oct", "Nov", "Dec"
	];

	var Note = Post.extend({
		defaults: {
			amountComment: 0,
			amountLike: 0,
			comments: [],
			likes: [],
			liked: false,
			tags: [],
			date: '',
			month: '',
			title: '',
			// commentPage default is 1
			// this is page number that we will request
			commentPage: 1
		},

		increaseCommentPage: function (amount) {
			this.attributes.commentPage += amount;
		},

		constructor: function (note, option) {
			note.amountComment = note.comments.length;
			note.amountLike = note.likes.length;
			// TODO did user like this
			note.liked = note.likes.indexOf(loggedInUser) === -1 ? false : true;
			// convert to Date object
			note.createdIn = new Date(note.createdIn);

			note.date = note.createdIn.getDate() < 10 ? '0' + note.createdIn.getDate() : note.createdIn.getDate();
			note.month = monthName[note.createdIn.getMonth()],

			// call constructor with custom note
			Backbone.Model.apply(this, [note, option]);
		}
	});

	return Note;
});