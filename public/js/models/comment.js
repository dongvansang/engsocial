define(['models/post'], function (Post) {
	var Comment;
	Comment = Post.extend({
		defaults: {
			nickname: 'Unknown',
			avatar: '/img/default-avatar.jpg',
			amountLike: 0,
			likes: [],
			liked: false,
			rate: 0,
			rateTotal: 0,
			rates: [],
			rated: false
		},

		initialize: function () {
			_.bindAll(this, 'manipulateRatingScore');
			this.bind('change:rates', this.manipulateRatingScore);
		},

		manipulateRatingScore: function (rates) {
			var len = rates.length,
				sum = 0;

			this.set('amountRate', len);
			_(rates).each(function (rate) {
				sum += rate.score || 0;
			});

			// 1.213 => 1.2
			if (sum === 0 || sum === NaN) {
				this.set('ratingTotalScore', 0);
			} else {
				this.set('ratingTotalScore', (sum/len).toFixed(1));
			}
		},

		constructor: function (comment, option) {
			comment.amountLike = comment.likes.length;
			comment.amountRate = comment.rates.length;
			comment.liked = comment.likes.indexOf(loggedInUser) === -1 ? false : true;

			// if you haven't yet rated, score is zero
			// else return your rating score
			var len = comment.rates.length;
			comment.rated = 0;
			var sum = 0;
			for (var i = 0; i < len; i++) {
				if (comment.rates[i].userId.toString() === loggedInUser.toString()) {
					comment.rated = comment.rates[i].score;
				}
				sum += comment.rates[i].score;
			};

			// 1.213 => 1.2
			if (sum === 0) {
				comment.ratingTotalScore = 0;
			} else {
				comment.ratingTotalScore = (sum/len).toFixed(1);
			}

			comment.liveTimestamp = date.dateConverter(new Date(comment.createdIn));

			Backbone.Model.apply(this, [comment, option]);
		}

	});

	return Comment
});