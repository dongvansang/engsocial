define(['Backbone'], function () {
	var ChatDock;
	ChatDock = Backbone.Model.extend({
		defaults: {
			id: 'unknown',
			sender: {
				id: 'me',
				nickname: 'unknown',
				avatar: '/img/default-avatar.jpg'
			},
			receiver: {
				id: 'unknown',
				nickname: 'unknown',
				avatar: '/img/default-avatar.jpg'
			},
			isOnline: false
		},

		// msgData: { msg, timestamp }
		addSendMsg: function (messageModel) {
			// push into messages array
			this.attributes.messages.add(messageModel);

			this.trigger('sendMsg', {
				senderId: this.attributes.sender.id,
				receiverId: this.attributes.receiver.id,
				msgData: {
					msg: messageModel.get('msg'),
					timestamp: messageModel.get('timestamp')
				}
			});
		},

		addMsg: function (messageModel) {
			this.attributes.messages.add(messageModel);
		}
	});

	return ChatDock;
});