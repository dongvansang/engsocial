define(['Backbone'], function () {
	var ChatUser;
	ChatUser = Backbone.Model.extend({
		defaults: {
			id: 'unknown',
			nickname: 'unknown',
			avatar: '/img/default-avatar.jpg',
			isOnline: false
		}
	});

	return ChatUser
});