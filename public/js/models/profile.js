define(['Backbone'], function () {
	var Profile;
	Profile = new Backbone.Model.extend({
		defaults: {
			nickname: 'Unknown',
			avatar: '/img/default-avatar.jpg',
			address: '',
			scoreObj: {
				score: 0,
				amount: 0
			}
		}
	});

	return Profile;
});