define(['Backbone'], function () {
	var Post = Backbone.Model.extend({
		urlRoot: '/api/post',	
		defaults: {
			nickname: 'Unknown',
			avatar: '/img/default-avatar.jpg',
			amountLike: 0,
			liked: false,
			likes: []
		}
	});

	return Post;
});