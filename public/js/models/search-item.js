define(['Backbone'], function () {
	var SearchItem = Backbone.Model.extend({
		constructor: function (searchItem, option) {
			Backbone.Model.apply(this, [searchItem, option]);
		}
	});

	return SearchItem;
});