define(['models/post', 'Backbone'], function (Post) {
	var Edit;
	Edit = Post.extend({
		defaults: {
			nickname: 'Unknown',
			avatar: '/img/default-avatar.jpg',
			editTo: '',
			editText: '',
			amountLike: 0,
			likes: [],
			liked: false,
			rate: 0,
			rateTotal: 0,
			rates: [],
			rated: false
		},

		initialize: function () {
			_.bindAll(this, 'manipulateRatingScore');
			this.bind('change:rates', this.manipulateRatingScore);
		},

		manipulateRatingScore: function (rates) {
			var len = rates.length,
				sum = 0;

			this.set('amountRate', len);
			_(rates).each(function (rate) {
				sum += rate.score || 0;
			});

			// 1.213 => 1.2
			if (sum === 0 || sum === NaN) {
				this.set('ratingTotalScore', 0);
			} else {
				this.set('ratingTotalScore', (sum/len).toFixed(1));
			}
		},

		constructor: function (edit, option) {
			edit.amountLike = edit.likes.length;
			edit.amountRate = edit.rates.length;

			// if you haven't yet rated, score is zero
			// else return your rating score
			var len = edit.rates.length;
			edit.rated = 0;
			var sum = 0;
			for (var i = 0; i < len; i++) {
				if (edit.rates[i].userId.toString() === loggedInUser.toString()) {
					edit.rated = edit.rates[i].score;
				}
				sum += edit.rates[i].score;
			};

			// 1.213 => 1.2
			if (sum === 0) {
				edit.ratingTotalScore = 0;
			} else {
				edit.ratingTotalScore = (sum/len).toFixed(1);
			}

			edit.liveTimestamp = date.dateConverter(new Date(edit.createdIn));
			
			Backbone.Model.apply(this, [edit, option]);
		}
	});

	return Edit;
});