define(['models/post', 'collections/comments', 'Backbone']
, function (Post, Comments) {
	var Status = Post.extend({
		defaults: {
			nickname: 'Unknown',
			avatar: '/img/default-avatar.jpg',
			comments: [],
			likes: [],
			rates: [],
			edits: [],
			amountComment: 0,
			amountLike: 0,
			amountRate: 0,
			amountEdit: 0,
			// commentPage default is 1
			// this is page number that we will request
			commentPage: 1,
			editPage: 1,
			liked: false,
			rated: false,
			rateTotal: 0
		},

		initialize: function () {
			_.bindAll(this, 'manipulateRatingScore');
			this.bind('change:rates', this.manipulateRatingScore);
		},

		increaseCommentPage: function (amount) {
			this.attributes.commentPage += amount;
		},

		increaseEditPage: function (amount) {
			this.attributes.editPage += amount;
		},

		manipulateRatingScore: function (rates) {
			var len = rates.length,
				sum = 0;

			this.set('amountRate', len);
			_(rates).each(function (rate) {
				sum += rate.score || 0;
			});

			// 1.213 => 1.2
			if (sum === 0 || sum === NaN) {
				this.set('ratingTotalScore', 0);
			} else {
				this.set('ratingTotalScore', (sum/len).toFixed(1));
			}
		},

		constructor: function (status, option) {
			status.amountComment = status.comments.length;
			status.amountLike = status.likes.length;
			status.amountRate = status.rates.length;
			status.amountEdit = status.edits.length;

			status.liked = status.likes.indexOf(loggedInUser) === -1 ? false : true;

			// if you haven't yet rated, score is zero
			// else return your rating score
			var len = status.rates.length;
			status.rated = 0;
			var sum = 0;
			for (var i = 0; i < len; i++) {
				if (status.rates[i].userId.toString() === loggedInUser.toString()) {
					status.rated = status.rates[i].score;
				}
				sum += status.rates[i].score;
			};

			// 1.213 => 1.2
			if (sum === 0) {
				status.ratingTotalScore = 0;
			} else {
				status.ratingTotalScore = (sum/len).toFixed(1);
			}

			status.createdInFormated = date.dateConverter(new Date(status.createdIn));

			Backbone.Model.apply(this, [status, option]);
		},

		addComment: function (comment) {
			this.get('comments').add(comment);
		},

		deleteComment: function (comment) {
			this.get('comments').remove(comment);
		}
	});

	return Status
});