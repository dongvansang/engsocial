define(['Backbone'], function () {
	var Message;
	Message = Backbone.Model.extend({
		defaults: {
			user: {
				nickname: 'unknown',
				avatar: '/img/default-avatar.jpg'
			},
			msg: '',
			timestamp: new Date()
		},

		constructor: function (message, option) {
			message.liveTimestamp = date.dateConverter(new Date(message.timestamp));
			Backbone.Model.apply(this, [message, option]);
		}
	});

	return Message;
});