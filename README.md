Dependencies package

 - express		: 3.0.6
 
 - jade			: 0.28.1
 
 - mongoose		: 3.5.7
 
 - crypto		: 0.0.3
 
 - async			: 0.2.6
 
 - underscore	: 1.4.4
 
 
Install dependencies package

$ sudo npm install


Run (listen on default port 8000)

$ node app
