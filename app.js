// var dbPath  = 'mongodb://sang-engsocial:1222@dharma.mongohq.com:10062/engsocialdb',
var dbPath, sio, express, http, path, _, app,
	mongoose, fs, helper, models, services,
	MemoryStore, sessionStore, flash, passport,
	LocalStrategy, sessionSecretString;
dbPath = 'mongodb://localhost/engsocialdb';
sio = require('socket.io');
express = require('express');
http = require('http');
path = require('path');
_ = require('underscore');
app = express();
mongoose = require('mongoose');
fs = require('fs');
helper = require('./core/helper')();
models = require('./models/models')(mongoose);
services = {
	user: require('./core/services/userService')(mongoose, models),
	newsfeed: require('./core/services/newsfeedService')(mongoose, models),
	status: require('./core/services/statusService')(mongoose, models),
	post: require('./core/services/postService')(mongoose, models),
	comment: require('./core/services/commentService')(mongoose, models),
	edit: require('./core/services/editService')(mongoose, models),
	note: require('./core/services/noteService')(mongoose, models),
	friend: require('./core/services/friendService')(mongoose, models),
	chat: require('./core/services/chatService')(mongoose, models)
};
MemoryStore = require('connect').session.MemoryStore;
sessionStore = new MemoryStore();
flash = require('connect-flash');
passport = require('passport');
LocalStrategy = require('passport-local').Strategy;
sessionSecretString = "The secret string";

passport.serializeUser(function(user, done) {
	done(null, { 
		id: user._id,
		nickname: user.nickname,
		avatar: user.avatar
	});
});

passport.deserializeUser(function(user, done) {
	models.User.findById(user.id, function (err, foundUser) {
		done(null, foundUser);
	});
});

passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'password'}
, function(email, password, done) {
	services.user.getUserByEmailPassword(email, helper.encryption.encrypt(password)
	, function (err, user) {
		if (err) {
			return done(err);
		} else {
			if (!user) {
				return done(null, false, { message: 'invalid email or password'});
			} else {
				return done(null, user);
			}
		}
	});
}));

app.configure(function() {
	app.PORT = process.env.PORT || 8000;
	app.helper = helper;
	app.rootDir = __dirname;

	// store users online
	app.usersOnline = [];

	app.set('views', __dirname + '/views');
	app.set('view engine', 'jade');
	// app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.session({
		secret: sessionSecretString,
		key: 'express.sid',
		store: sessionStore
	}));
	app.use(express.static(path.join(__dirname, 'public')));
	
	app.use(flash());
	app.use(passport.initialize());
	app.use(passport.session());
});

function loadRouteDir(routePath) {
	fs.readdirSync(routePath).forEach(function (file) {
		var routeName = file.substr(0, file.indexOf('.'));
		require('./' + routePath + '/' + routeName)(app, services, passport);
	});
}

loadRouteDir('routes');
loadRouteDir('routes/api');

console.log('Conneting database...');
// mongoose config
mongoose.connect(dbPath, function (err) {
	if (err) {
		console.log(err);
	} else {
		console.log('Connect database successfully');

		// start server
		http.createServer(app).listen(app.PORT, function() {
			console.log('Engsocial server listening on port ' + app.PORT);

			// start websocket server
			var sio = require('socket.io').listen(this);
			require('./core/websocket')(services.chat, services.friend, sio, sessionStore, sessionSecretString, app);
		});
	}
});

process.on('uncaughtException', function(err) {
  console.log(err);
});